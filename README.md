# FIRMU #

Imagine that you want to find out which movies that are available in the cinema nearby you.
And you don't want to go to the website of the cinema all the time. But you want to find out which movies are available in you country.

By finding that specific movie you find in interest. You choose that one and get a list of all the cinemas that are showing the movie at what current time based on today.

Firmu offers also provide trailer for the movie which is streamed through YouTube.
The app also has Google Map functionality to mark which cinemas a that are nearby you and check your current place and center your location.

Features:
- Search for movies
- find movies that are in cinema and which cinema is near you.
- get details of movies and actors
- watch trailers streamed from YouTube
- save movie as a watchlist/favourite

Sources for movies and poster information is used from https://www.themoviedb.org.