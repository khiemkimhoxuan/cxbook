package com.computas.kkho.firmu.ui;

import android.support.v4.app.Fragment;

/**
 * Created by kkho on 17.05.2015.
 */
public class SearchResultActivity extends BaseSupportActivity {
    String query;

    @Override
    protected Fragment onCreatePane() {
        setTitle("Search Results");
        return new SearchResultFragment();
    }
}
