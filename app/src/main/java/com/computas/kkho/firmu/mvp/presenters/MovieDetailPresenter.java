package com.computas.kkho.firmu.mvp.presenters;

import android.graphics.Bitmap;
import android.os.Handler;
import android.text.TextUtils;

import com.computas.kkho.firmu.domain.controllers.GetMovieDetailUseCaseController;
import com.computas.kkho.firmu.model.Credits;
import com.computas.kkho.firmu.model.MovieDetail;
import com.computas.kkho.firmu.model.MovieTrailer;
import com.computas.kkho.firmu.model.ReviewWrapper;
import com.computas.kkho.firmu.mvp.views.DetailView;
import com.computas.kkho.firmu.rest.RestMovieSource;
import com.computas.kkho.firmu.ui.HomeFragment;
import com.computas.kkho.firmu.ui.SearchResultFragment;
import com.computas.kkho.util.BusProvider;
import com.squareup.otto.Subscribe;

/**
 * Created by kkh on 26.03.2015.
 */
public class MovieDetailPresenter extends Presenter {
    private final DetailView mMovieDetailView;
    private final String mMovieID;

    public MovieDetailPresenter(DetailView movieDetailView, String movieID) {
        mMovieDetailView = movieDetailView;
        mMovieID = movieID;
        Bitmap bitmap = HomeFragment.sPhotoCache.get(0) == null ? SearchResultFragment.sPhotoCache.get(0)
                : HomeFragment.sPhotoCache.get(0);
        mMovieDetailView.showFilmCover(bitmap);

        new GetMovieDetailUseCaseController(mMovieID,
                BusProvider.getUIBusInstance(), RestMovieSource.getInstance()).execute();
    }

    public void showDescription(String description) {
        mMovieDetailView.setDescription(description);
    }

    @Override
    public void start() {
        BusProvider.getUIBusInstance().register(this);
    }

    @Override
    public void stop() {
        BusProvider.getUIBusInstance().unregister(this);
    }

    public void showTagline(String tagLine) {
        mMovieDetailView.setTagline(tagLine);
    }

    public void showTitle(String title) {
        mMovieDetailView.setName(title);
    }

    @Subscribe
    public void onDetailInformationReceived(MovieDetail response) {

        showDescription(response.getOverview());
        showTitle(response.getTitle());
        showTagline(response.getTagline());
        showHomepage(response.getHomepage());
        showAverageRating(response.getVote_average() + "");

    }

    @Subscribe
    public void onReviewsReceived(final ReviewWrapper reviewsWrapper) {

        // Wait 300 milliseconds to ensure that Palette generate the colors
        // before show the reviews
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (reviewsWrapper.getResults().size() > 0)
                    mMovieDetailView.showReviews(reviewsWrapper.getResults());
            }
        }, 300);

    }

    @Subscribe
    public void onCreditsReceived(final Credits response) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!response.getCast().isEmpty())
                    mMovieDetailView.showCredits(response.getCast());
            }
        }, 300);
    }

    @Subscribe
    public void onMovieTrailerReceived(final MovieTrailer response) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (response != null)
                    mMovieDetailView.showMovieTrailer(response);
            }
        }, 300);
    }

    public void showHomepage(String homepage) {

        if (!TextUtils.isEmpty(homepage))
            mMovieDetailView.setHomepage(homepage);
    }

    public void showAverageRating(String rating) {
        if (!TextUtils.isEmpty(rating)) {
            mMovieDetailView.setAverageRating(rating);
        }
    }
}
