package com.computas.kkho.firmu.model;


import android.content.ContentValues;

import com.computas.kkho.firmu.storage.NewsFeedContract;

import java.io.Serializable;


/**
 * Created by kkh on 05.03.2015.
 */
public class BoxOfficeMovie implements Serializable, Comparable<BoxOfficeMovie> {

    private String posterUrl;

    private String duration;

    private String showTime;


    private String adult;
    private String backdrop_path;
    private String id;
    private String original_title;
    private String release_date;
    private String poster_path;
    private String popularity;
    private String title;
    private String nonStrippedTitle;
    private String vote_average;
    private String vote_count;
    private String overview;
    private boolean movieReady;

    public BoxOfficeMovie() {
    }

    public BoxOfficeMovie(String id, String title, String overview) {

        this.id = id;
        this.title = title;
        this.overview = overview;
    }

    public String getAdult() {

        return adult;
    }

    public String getBackdrop_path() {

        return backdrop_path;
    }

    public void setId(String newId) {
        this.id = newId;
    }

    public String getId() {

        return id;
    }

    public String getOriginal_title() {

        return original_title;
    }

    public String getRelease_date() {

        return release_date;
    }

    public String getPoster_path() {

        return poster_path;
    }

    public String getPopularity() {

        return popularity;
    }


    public String getVote_average() {

        return vote_average;
    }

    public String getVote_count() {

        return vote_count;
    }

    public void setMovieReady(boolean movieReady) {

        this.movieReady = movieReady;
    }

    public boolean isMovieReady() {

        return movieReady;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getTitle() {
        return title;
    }

    public String getNonStripptedTitle() {
        return nonStrippedTitle;
    }

    public void setNonStrippedTitle(String title) {
        nonStrippedTitle = title;
    }

    public String getDuration() {
        return duration;
    }

    public String getShowTime() {
        return showTime;
    }

    public void setShowTime(String showTime) {
        this.showTime = showTime;
    }

    public static ContentValues getContentValuesForModel(BoxOfficeMovie mBoxOfficeMovie) {
        ContentValues values = new ContentValues(2);
        values.put(NewsFeedContract.MovieDB.ID, mBoxOfficeMovie.getId());
        values.put(NewsFeedContract.MovieDB.TITLE, mBoxOfficeMovie.getTitle());
        return values;
    }

    @Override
    public int compareTo(BoxOfficeMovie another) {
        return this.getId().compareTo(another.getId());
    }
}
