package com.computas.kkho.firmu.adapter;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.computas.kkho.firmu.R;
import com.computas.kkho.firmu.model.BoxOfficeMovie;
import com.computas.kkho.firmu.util.ColorUtil;
import com.computas.kkho.firmu.util.RecyclerViewClickListener;
import com.computas.kkho.util.MovieDbApiUrlInfo;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.Iterator;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by kkho on 31.05.2015.
 */
public class NewsFeedAdapter extends RecyclerView.Adapter<NewsFeedAdapter.NewsFeedViewHolder> {
    List<BoxOfficeMovie> mBoxOfficeMovies;
    Context mContext;
    private RecyclerViewClickListener mRecyclerClickListener;

    public NewsFeedAdapter(Context context, List<BoxOfficeMovie> boxOfficeMovies) {
        mBoxOfficeMovies = boxOfficeMovies;
        mContext = context;
    }

    public void setRecyclerListListener(RecyclerViewClickListener mRecyclerClickListener) {
        this.mRecyclerClickListener = mRecyclerClickListener;
    }

    @Override
    public NewsFeedViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View mItemView = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.partial_movie_item_list, viewGroup, false);
        return new NewsFeedViewHolder(mItemView, mRecyclerClickListener);
    }


    @Override
    public void onBindViewHolder(final NewsFeedViewHolder newsFeedViewHolder, final int position) {
        final BoxOfficeMovie mBoxOfficeMovie = mBoxOfficeMovies.get(position);
        final String posterURL = MovieDbApiUrlInfo.BASIC_STATIC_URL + mBoxOfficeMovie.getPoster_path();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            newsFeedViewHolder.vPosterImage.setTransitionName("cover" + position);

        Picasso.with(mContext)
                .load(posterURL)
                .fit().centerInside()
                .memoryPolicy(MemoryPolicy.NO_STORE)
                .placeholder(R.drawable.video_default)
                .into(newsFeedViewHolder.vPosterImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        mBoxOfficeMovie.setMovieReady(true);
                        newsFeedViewHolder.vMovieProgressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        Picasso.with(mContext).invalidate(posterURL);
                    }
                });
    }

    @Override
    public int getItemCount() {
        return mBoxOfficeMovies.size();
    }

    public List<BoxOfficeMovie> getMovieList() {
        return mBoxOfficeMovies;
    }

    public void appendMovies(List<BoxOfficeMovie> movieList) {
        Iterator<BoxOfficeMovie> itr = movieList.iterator();
        BoxOfficeMovie boxOfficeMovie;
        while (itr.hasNext()) {
            boxOfficeMovie = itr.next();
            if (!mBoxOfficeMovies.contains(boxOfficeMovie)) {
                mBoxOfficeMovies.add(boxOfficeMovie);
            }
        }
        notifyDataSetChanged();
    }

    class NewsFeedViewHolder extends RecyclerView.ViewHolder implements View.OnTouchListener {
        private final RecyclerViewClickListener onClickListener;
        @InjectView(R.id.movie_progressbar)
        ProgressBar vMovieProgressBar;
        @InjectView(R.id.ivPosterImage)
        ImageButton vPosterImage;

        public NewsFeedViewHolder(View v, RecyclerViewClickListener onClickListener) {
            super(v);
            ButterKnife.inject(this, v);
            vPosterImage.setDrawingCacheEnabled(true);
            vPosterImage.setOnTouchListener(this);
            ColorUtil.colorProgressBar(vMovieProgressBar, mContext);
            this.onClickListener = onClickListener;
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_UP && event.getAction() != MotionEvent.ACTION_MOVE) {
                onClickListener.onClick(v, getLayoutPosition(), event.getX(), event.getY());
            }
            return true;
        }
    }
}