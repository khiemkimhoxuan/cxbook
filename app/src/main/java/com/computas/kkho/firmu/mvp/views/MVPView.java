package com.computas.kkho.firmu.mvp.views;

/**
 * Created by kkh on 26.03.2015.
 */
public interface MVPView {
    public android.content.Context getContext();
}
