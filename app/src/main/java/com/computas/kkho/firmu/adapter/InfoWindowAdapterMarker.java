package com.computas.kkho.firmu.adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.computas.kkho.firmu.R;
import com.computas.kkho.firmu.util.ColorUtil;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by kkh on 16.05.2015.
 */
public class InfoWindowAdapterMarker implements GoogleMap.InfoWindowAdapter {
    private Marker mMarkerShowingInfoWindow;
    private Context mContext;
    private int mBackgroundColor;

    public InfoWindowAdapterMarker(Context context, int backgroundColor) {
        mContext = context;
        mBackgroundColor = backgroundColor;
    }

    @Override
    public View getInfoContents(Marker marker) {

        mMarkerShowingInfoWindow = marker;

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // Getting view from the layout file info_window_layout
        View popUp = inflater.inflate(R.layout.markerpopup, null);

        ImageView popupImageCinema = (ImageView) popUp.findViewById(R.id.cinemanameLabel_popup);
        TextView popUpTitle = (TextView) popUp.findViewById(R.id.title_popup);
        TextView popUpSnippet = (TextView) popUp.findViewById(R.id.address_popup);
        ImageView popupLabelAddressImage = (ImageView) popUp.findViewById(R.id.addresslabel_popup);
        TextView popupTimeInfo = (TextView) popUp.findViewById(R.id.text_display_time_popup);
        ImageView timeImageViewTime = (ImageView) popUp.findViewById(R.id.display_time_popup_image);


        popUpTitle.setText(marker.getTitle());
        if (!marker.getTitle().equals("My Position")) {
            if (marker.getSnippet().contains("#TIME:")) {
                String snippets[] = marker.getSnippet().split("#TIME:");
                popUpSnippet.setText(snippets[0]);
                popupTimeInfo.setText(snippets[1]);
            } else {
                popUpSnippet.setText(marker.getSnippet());
                timeImageViewTime.setVisibility(View.GONE);
            }
        } else {
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                popupImageCinema.setImageDrawable(mContext.getResources().getDrawable(
                        R.drawable.ic_group_black_24dp, mContext.getTheme()));
            } else {
                popupImageCinema.setImageDrawable(mContext.getResources().getDrawable(
                        R.drawable.ic_group_black_24dp));
            }
            popUpSnippet.setVisibility(View.GONE);
            popupLabelAddressImage.setVisibility(View.GONE);
            timeImageViewTime.setVisibility(View.GONE);
            popupTimeInfo.setVisibility(View.GONE);
        }

        if (mBackgroundColor != -1) {
            ColorUtil.colorImage(timeImageViewTime, mContext, mBackgroundColor);
            ColorUtil.colorImage(popupImageCinema, mContext, mBackgroundColor);
            ColorUtil.colorImage(popupLabelAddressImage, mContext, mBackgroundColor);
        }


        // Returning the view containing InfoWindow contents
        return popUp;
    }

    private View.OnClickListener popupClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {


        }
    };

    @Override
    public View getInfoWindow(Marker marker) {

        return null;
    }

    private void goToCinemaList(View v) {
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}

