package com.computas.kkho.firmu.ui;


import android.support.v4.app.Fragment;

/**
 * Created by kkh on 20.03.2015.
 */
public class GeoMapActivity extends BaseSupportActivity {


    @Override
    protected Fragment onCreatePane() {
        return new GeoMapFragment();
    }
}
