package com.computas.kkho.firmu.ui;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.computas.kkho.firmu.Firmu;
import com.computas.kkho.firmu.R;
import com.computas.kkho.firmu.adapter.CinemaAdapter;
import com.computas.kkho.firmu.model.Theater;
import com.computas.kkho.firmu.storage.NewsFeedContract;
import com.computas.kkho.firmu.util.ColorUtil;
import com.computas.kkho.firmu.util.Constants;
import com.squareup.leakcanary.RefWatcher;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by kkh on 18.04.2015.
 */
public class TheaterInfoFragment extends android.support.v4.app.Fragment implements
        LoaderManager.LoaderCallbacks<Cursor> {

    ArrayList<Theater> mTheaters = new ArrayList<>();

    @InjectView(R.id.recycler_view_cinema_list)
    RecyclerView mRecyclerViewCinemas;
    @InjectView(R.id.cinema_progressbar)
    ProgressBar mCinemaProgressBar;

    CinemaAdapter mCinemaAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
        getActivity().getLoaderManager().restartLoader(0, null, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cinema_recyclerview, container, false);
        ButterKnife.inject(this, view);
        ColorUtil.colorProgressBar(mCinemaProgressBar, getActivity());
        initRecyclerView(mTheaters);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onDetach() {
        getActivity().getContentResolver().unregisterContentObserver(mObserver);
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        RefWatcher refWatcher = Firmu.getRefWatcher(getActivity());
        refWatcher.watch(this);
        if (mCinemaAdapter != null) {
            mCinemaAdapter.notifyItemRangeRemoved(0, mCinemaAdapter.getItemCount());
            mCinemaAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().getContentResolver().registerContentObserver(
                NewsFeedContract.TheaterDB.CONTENT_URI, true, mObserver);
    }

    private final ContentObserver mObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            if (getActivity() == null) {
                return;
            }
            Loader<Cursor> loader = getActivity().getLoaderManager().getLoader(0);
            if (loader != null) {
                loader.forceLoad();
            }
        }
    };

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(),
                NewsFeedContract.TheaterDB.CONTENT_URI,
                Constants.TheaterQuery.PROJECTION,
                null,
                null,
                null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (getActivity() == null) return;
        while (data.moveToNext()) {
            Theater theater = new Theater();
            theater.setName(data.getString(Constants.TheaterQuery.NAME));
            theater.setInfo(data.getString(Constants.TheaterQuery.INFO));
            String moviesShown = data.getString(Constants.TheaterQuery.MOVIES);
            String movieInfoTime = data.getString(Constants.TheaterQuery.MOVIESTIME);
            theater.setMoviesToShow(moviesShown);
            theater.setMovieShowTimeInfo(movieInfoTime);
            mTheaters.add(theater);
        }
        mCinemaAdapter.notifyDataSetChanged();
        mCinemaProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    private void initRecyclerView(ArrayList<Theater> cinemaList) {
        mCinemaAdapter = new CinemaAdapter(getActivity(), cinemaList, mCinemaProgressBar);
        mRecyclerViewCinemas.setHasFixedSize(true);
        mRecyclerViewCinemas.setAdapter(mCinemaAdapter);
    }

    private void showLoading() {
        mCinemaProgressBar.setVisibility(View.VISIBLE);
        mCinemaProgressBar.getIndeterminateDrawable().
                setColorFilter(getResources().getColor(R.color.theme_color_darker), android.graphics.PorterDuff.Mode.MULTIPLY);
    }
}
