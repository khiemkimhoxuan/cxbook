package com.computas.kkho.firmu.storage;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by kkh on 05.03.2015.
 */
public class NewsFeedContract {
    public static final String CONTENT_AUTHORITY = "com.awesome.kkho.firmu";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String TABLE_MOVIE = "movies";
    public static final String TABLE_THEATERS = "theaters";

    interface MovieColumns {
        String ID = "movieid";
        String TITLE = "title";
    }

    interface TheaterColumns {
        String NAME = "name";
        String INFO = "info";
        String MOVIES = "movies";
        String MOVIESTIME = "movietime";
    }


    public static class MovieDB implements MovieColumns, BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(TABLE_MOVIE).build();
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.firmu.movies";
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.firmu.movies";

        public static Uri buildUri(String id) {
            return BASE_CONTENT_URI.buildUpon().appendPath(id).build();
        }
    }

    public static class TheaterDB implements TheaterColumns, BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(TABLE_THEATERS).build();
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.firmu.theaters";
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.firmu.theaters";

        public static Uri buildUri(String id) {
            return BASE_CONTENT_URI.buildUpon().appendPath(id).build();
        }
    }

}
