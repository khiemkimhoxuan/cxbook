package com.computas.kkho.firmu.ui;


import android.support.v4.app.Fragment;

/**
 * Created by kkho on 18.03.2015.
 */
public class ItemMovieActivity extends BaseSupportActivity {

    @Override
    protected Fragment onCreatePane() {
        return new ItemMovieFragment();
    }
}
