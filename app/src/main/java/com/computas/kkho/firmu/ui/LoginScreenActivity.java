package com.computas.kkho.firmu.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import com.computas.kkho.firmu.R;

/**
 * Created by kkho on 16.03.2015.
 */
public class LoginScreenActivity extends Activity {
    private Typeface mFont;
    private TextView mSplashText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);
        mSplashText = (TextView) findViewById(R.id.splash_screen_text);
        mFont = Typeface.createFromAsset(getAssets(), "roboto/Roboto-Bold.ttf");
        mSplashText.setTypeface(mFont);
        //Get login information stored
        //Then simulate startscreen
        new Thread(new Runnable() {
            @Override
            public void run() {
                //get login information
            }
        }).start();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(intent);
                finish();
            }
        }, 1200);
    }


}
