package com.computas.kkho.firmu.domain.controllers;

import android.content.Context;

import com.computas.kkho.firmu.domain.GetMoviesUseCase;
import com.computas.kkho.firmu.model.BoxOfficeMovieWrapper;
import com.computas.kkho.firmu.model.BoxOfficeMovieWrapperNowPlaying;
import com.computas.kkho.firmu.model.BoxOfficeSearchMovieWrapper;
import com.computas.kkho.firmu.model.DiscoverWrapper;
import com.computas.kkho.firmu.rest.RestDataSource;
import com.computas.kkho.firmu.util.DateUtil;
import com.computas.kkho.util.BusProvider;
import com.google.gson.Gson;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

/**
 * Created by kkh on 26.03.2015.
 */
public class GetMoviesUsecaseController implements GetMoviesUseCase {

    private final RestDataSource mDataSource;
    private final Bus mUIBus;
    private Context mContext;
    private static int mCurrentPage = 1;
    private static int mCurrentPopularMoviePage = 1;
    private static int mCurrentDiscoverPage = 1;
    private static int mCurrentSearchedMoviePage = 1;
    private int mMaxPage = Integer.MAX_VALUE;
    private Gson gson;

    public GetMoviesUsecaseController(Context context, RestDataSource dataSource, Bus UIBus) {
        mDataSource = dataSource;
        mUIBus = UIBus;
        mContext = context;
        gson = new Gson();
        BusProvider.getRestBusInstance().register(this);
    }

    @Subscribe
    @Override
    public void onPopularMoviesReceived(BoxOfficeMovieWrapper response) {
        sendMoviesToPresenter(response);
    }

    @Subscribe
    public void onSearchedMoviesReceived(BoxOfficeSearchMovieWrapper response) {
        mMaxPage = response.getTotal_pages().intValue();
        sendSearchedMoviesToPresenter(response);
    }

    @Override
    public void requestPopularMovies() {
        mDataSource.getMoviesByPage(mCurrentPopularMoviePage);
        mCurrentPopularMoviePage++;
    }

    @Override
    public boolean requestSearchMovies(String query) {
        if (mCurrentSearchedMoviePage <= mMaxPage) {
            mDataSource.getSearchedMovieByPage(query, mCurrentSearchedMoviePage);
            mCurrentSearchedMoviePage++;
            return true;
        }
        return false;
    }

    @Subscribe
    @Override
    public void onNowPlayingMoviesReceived(BoxOfficeMovieWrapperNowPlaying response) {
        mUIBus.post(response);
        mCurrentPage++;

    }

    @Subscribe
    @Override
    public void onDiscoverMoviesReceived(DiscoverWrapper response) {
        sendDiscoverMoviesToPresenter(response);
    }

    @Override
    public void requestNowPlayingMovies() {
        mDataSource.getNowPlayingMovieByPage(mCurrentPage);
    }

    @Override
    public void requestDiscoverMovies() {
        mDataSource.getDiscoveryMoviesByPage(mCurrentDiscoverPage, DateUtil.getDateInstance());
        mCurrentDiscoverPage++;
    }

    @Override
    public void sendNowPlayingMoviesToPresenter(BoxOfficeMovieWrapperNowPlaying response) {
        mUIBus.post(response);
    }

    @Override
    public void sendDiscoverMoviesToPresenter(DiscoverWrapper response) {
        mUIBus.post(response);
    }

    @Override
    public void sendMoviesToPresenter(BoxOfficeMovieWrapper response) {
        mUIBus.post(response);
    }

    public void sendSearchedMoviesToPresenter(BoxOfficeSearchMovieWrapper response) {
        mUIBus.post(response);
    }

    @Override
    public void unRegister() {
        BusProvider.getRestBusInstance().unregister(this);

    }

    public void executeRefresh() {
        mCurrentDiscoverPage = 1;
        requestDiscoverMovies();
    }

    public void resetRequestPage() {
        mCurrentDiscoverPage = 1;
        mCurrentPopularMoviePage = 1;
        mCurrentSearchedMoviePage = 1;

    }

    public void executeSearchRefresh() {
        mCurrentSearchedMoviePage = 1;
    }

    @Override
    public void execute() {
        //requestPopularMovies();
        //requestSearchMovies();
        requestDiscoverMovies();

    }
}
