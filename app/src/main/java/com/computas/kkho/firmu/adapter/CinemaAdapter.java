package com.computas.kkho.firmu.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.computas.kkho.firmu.R;
import com.computas.kkho.firmu.model.Theater;
import com.computas.kkho.firmu.ui.CinemaMovieInfoActivity;
import com.computas.kkho.firmu.ui.GeoMapActivity;
import com.computas.kkho.firmu.util.Constants;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by kkh on 18.04.2015.
 */
public class CinemaAdapter extends RecyclerView.Adapter<CinemaAdapter.CinemaViewHolder> {
    ArrayList<Theater> mTheaters;
    Context mContext;
    ProgressBar mProgressBar;

    public CinemaAdapter(Context context, ArrayList<Theater> theaters, ProgressBar progressBar) {
        mTheaters = theaters;
        mContext = context;
        mProgressBar = progressBar;
    }

    @Override
    public CinemaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mItemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_cinema_info, parent, false);
        return new CinemaViewHolder(mItemView);
    }

    @Override
    public int getItemCount() {
        return mTheaters.size();
    }

    @Override
    public void onBindViewHolder(CinemaViewHolder holder, int position) {
        String infoSplit[];
        String nameAssumption;
        String phoneNumber;
        Theater theater = mTheaters.get(position);
        holder.vCinemaName.setText(theater.getName());
        infoSplit = theater.getInfo().split(" - ");

        nameAssumption = infoSplit[0];
        if (infoSplit.length > 1) {
            phoneNumber = infoSplit[1];
            if (infoSplit.length > 2) {
                nameAssumption += ", " + infoSplit[1];
                phoneNumber = infoSplit[2];
            }
            holder.vCinemaPhoneNumber.setText(phoneNumber);
        }
        holder.vCinemaAddress.setText(nameAssumption);
        holder.setTheater(theater);
        mProgressBar.setVisibility(View.GONE);
    }

    class CinemaViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.txt_name)
        TextView vCinemaName;
        @InjectView(R.id.txt_address)
        TextView vCinemaAddress;
        @InjectView(R.id.txt_phone)
        TextView vCinemaPhoneNumber;
        @InjectView(R.id.map_location_image)
        ImageView vMapImageView;
        @InjectView(R.id.movie_list_image)
        ImageView vMovieIconImageView;

        Theater vCurrentTheater;

        public CinemaViewHolder(View v) {
            super(v);
            ButterKnife.inject(this, v);
            colorMapImage();
        }

        public void colorMapImage() {
            Resources res = mContext.getResources();
            int primaryColor = res.getColor(R.color.theme_color);
            vMapImageView.setColorFilter(primaryColor, PorterDuff.Mode.SRC_ATOP);
            vMovieIconImageView.setColorFilter(primaryColor, PorterDuff.Mode.SRC_ATOP);
        }

        @OnClick({R.id.map_location_image, R.id.movie_list_image})
        void goToCinemaInfoClickListener(View view) {
            Intent intent = null;
            if (view.getId() == R.id.map_location_image) {
                intent = new Intent(mContext, GeoMapActivity.class);
                intent.putExtra(Constants.CINEMA_LOCATION, vCurrentTheater);
            } else if (view.getId() == R.id.movie_list_image) {
                intent = new Intent(mContext, CinemaMovieInfoActivity.class);
                intent.putExtra(Constants.MOVIE_PLAYING_CINEMA, vCurrentTheater);
            }
            mContext.startActivity(intent);

        }

        public void setTheater(Theater theater) {
            vCurrentTheater = theater;
        }
    }
}
