package com.computas.kkho.firmu.ui;

import android.support.v4.app.Fragment;

/**
 * Created by kkh on 18.04.2015.
 */
public class TheaterInfoActivity extends BaseSupportActivity {

    @Override
    protected Fragment onCreatePane() {
        return new TheaterInfoFragment();
    }
}
