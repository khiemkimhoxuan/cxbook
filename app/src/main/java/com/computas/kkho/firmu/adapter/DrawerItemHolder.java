package com.computas.kkho.firmu.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.computas.kkho.firmu.R;
import com.computas.kkho.firmu.util.ColorUtil;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by kkh on 12.05.2015.
 */
public class DrawerItemHolder extends RecyclerView.ViewHolder {
    @InjectView(R.id.drawer_item_icon)
    ImageView vDrawerIcon;
    @InjectView(R.id.drawer_item_text)
    TextView vDrawerText;

    public DrawerItemHolder(View view) {
        super(view);
        ButterKnife.inject(this, view);
        ColorUtil.colorIcons(vDrawerIcon, view.getContext(), android.R.color.white);
        view.setClickable(true);
    }
}