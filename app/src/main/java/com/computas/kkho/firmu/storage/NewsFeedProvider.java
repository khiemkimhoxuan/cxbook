package com.computas.kkho.firmu.storage;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.computas.kkho.firmu.util.SelectionBuilder;

import java.util.ArrayList;

/**
 * Created by kkh on 05.03.2015.
 */
public class NewsFeedProvider extends ContentProvider {

    private NewsFeedDatabase mOpenHelper;
    private static final UriMatcher sUriMatcher = buildUriMatcher();

    private static final int MOVIES = 100;
    private static final int MOVIE_ID = 101;
    private static final int THEATERS = 200;
    private static final int THEATER_ID = 201;

    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = NewsFeedContract.CONTENT_AUTHORITY;

        matcher.addURI(authority, "movies", MOVIES);
        matcher.addURI(authority, "movies/*", MOVIE_ID);
        matcher.addURI(authority, "theaters", THEATERS);
        matcher.addURI(authority, "theaters/*", THEATER_ID);

        return matcher;
    }

    @Override
    public boolean onCreate() {
        mOpenHelper = new NewsFeedDatabase(getContext());
        return false;
    }

    @Override
    public String getType(Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case MOVIES:
                return NewsFeedContract.MovieDB.CONTENT_TYPE;
            case MOVIE_ID:
                return NewsFeedContract.MovieDB.CONTENT_ITEM_TYPE;
            case THEATERS:
                return NewsFeedContract.TheaterDB.CONTENT_TYPE;
            case THEATER_ID:
                return NewsFeedContract.TheaterDB.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        final SQLiteDatabase db = mOpenHelper.getReadableDatabase();

        final int match = sUriMatcher.match(uri);
        switch (match) {
            default: {
                // Most cases are handled with simple SelectionBuilder
                final SelectionBuilder builder = buildSimpleSelection(uri);
                return builder.where(selection, selectionArgs).query(db,
                        projection, sortOrder);
            }
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        final String ID;
        final String THEATER_NAME;
        ContentResolver contentResolver = getContext().getContentResolver();
        switch (match) {
            case MOVIES: {
                ID = NewsFeedContract.MovieDB.ID;
                Cursor c = checkIfMovieOrTheaterNotExists(ID, uri,
                        values, contentResolver);
                if (c.getCount() == 0) {
                    db.insertOrThrow(NewsFeedContract.TABLE_MOVIE, null, values);
                }
                contentResolver.notifyChange(uri, null);
                c.close();
                return NewsFeedContract.MovieDB.buildUri(
                        values.getAsString(NewsFeedContract.MovieDB._ID));
            }

            case THEATERS: {
                THEATER_NAME = NewsFeedContract.TheaterDB.NAME;
                Cursor c = checkIfMovieOrTheaterNotExists(THEATER_NAME, uri,
                        values, contentResolver);
                if (c.getCount() == 0) {
                    db.insertOrThrow(NewsFeedContract.TABLE_THEATERS, null, values);
                } else {
                    db.update(NewsFeedContract.TABLE_THEATERS, values, THEATER_NAME + "=?",
                            new String[]{THEATER_NAME});
                }
                contentResolver.notifyChange(uri, null);
                c.close();
                return NewsFeedContract.TheaterDB.buildUri(
                        values.getAsString(NewsFeedContract.TheaterDB._ID));

            }
            default: {
                throw new UnsupportedOperationException("Unknown uri: " + uri);
            }
        }
    }

    private Cursor checkIfMovieOrTheaterNotExists(final String name, Uri uri,
                                                  ContentValues values, ContentResolver contentResolver) {
        return contentResolver.query(uri, null,
                name + " = " +
                        DatabaseUtils.sqlEscapeString(values.getAsString(name)), null, null);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        if (uri == NewsFeedContract.BASE_CONTENT_URI) {
            // Handle whole database deletes (e.g. when signing out)
            return 1;
        }
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final SelectionBuilder builder = buildSimpleSelection(uri);
        int retVal = builder.where(selection, selectionArgs).delete(db);
        getContext().getContentResolver().notifyChange(uri, null);
        return retVal;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final SelectionBuilder builder = buildSimpleSelection(uri);
        int retVal = builder.where(selection, selectionArgs).update(db, values);

        getContext().getContentResolver().notifyChange(uri, null);
        return retVal;
    }


    /**
     * Apply the given set of {@link android.content.ContentProviderOperation}, executing inside
     * a {@link SQLiteDatabase} transaction. All changes will be rolled back if
     * any single one fails.
     */
    @Override
    public ContentProviderResult[] applyBatch(
            @NonNull ArrayList<ContentProviderOperation> operations)
            throws OperationApplicationException {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            final int numOperations = operations.size();
            final ContentProviderResult[] results = new ContentProviderResult[numOperations];
            for (int i = 0; i < numOperations; i++) {
                results[i] = operations.get(i).apply(this, results, i);
            }
            db.setTransactionSuccessful();
            return results;
        } finally {
            db.endTransaction();
        }
    }

    /**
     * Build a simple {@link SelectionBuilder} to match the requested
     * {@link Uri}. This is usually enough to support {@link #insert},
     * {@link #update}, and {@link #delete} operations.
     */
    private SelectionBuilder buildSimpleSelection(Uri uri) {
        final SelectionBuilder builder = new SelectionBuilder();
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case MOVIES: {
                return builder.table(NewsFeedContract.TABLE_MOVIE);
            }
            case MOVIE_ID: {

            }
            case THEATERS: {
                return builder.table(NewsFeedContract.TABLE_THEATERS);
            }
            case THEATER_ID: {

            }
            default: {
                throw new UnsupportedOperationException("Unknown uri: " + uri);
            }
        }
    }
}
