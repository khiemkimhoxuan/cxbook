package com.computas.kkho.firmu.model;

/**
 * Created by kkh on 14.05.2015.
 */
public class TheaterMovie {
    private String movieName;
    private String showTime;

    public TheaterMovie(String movieName, String showTime) {
        this.movieName = movieName;
        this.showTime = showTime;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public String getShowTime() {
        return showTime;
    }

    public void setShowTime(String showTime) {
        this.showTime = showTime;
    }
}
