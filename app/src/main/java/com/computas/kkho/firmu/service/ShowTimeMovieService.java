package com.computas.kkho.firmu.service;

import android.app.IntentService;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.RemoteException;
import android.text.TextUtils;

import com.computas.kkho.firmu.model.BoxOfficeMovie;
import com.computas.kkho.firmu.model.Theater;
import com.computas.kkho.firmu.storage.NewsFeedContract;
import com.computas.kkho.firmu.util.CinemaUtil;
import com.google.android.gms.location.FusedLocationProviderApi;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by kkh on 17.03.2015.
 */
public class ShowTimeMovieService extends IntentService {

    private static final String SHOWTIME_URI = "http://google.com/movies";
    //"&page_limit=" + MOVIE_PAGE_LIMIT
    private static final String THEATER_NAME = "name";
    private static final String THEATER_INFO = "info";
    private static final String THEATER_MOVIE = "movie";
    private static Location currentLocation;
    private static int PAGE_LIMIT = 0;
    private static List<Address> addresses = null;

    public ShowTimeMovieService() {
        super("ShowTimeMovieService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        currentLocation = (Location) intent.getExtras().get(FusedLocationProviderApi.KEY_LOCATION_CHANGED);
        if (currentLocation != null) {
            Geocoder gcd = new Geocoder(getApplicationContext(), Locale.getDefault());
            if (Geocoder.isPresent()) {
                try {
                    addresses = gcd.getFromLocation(currentLocation.getLatitude(), currentLocation.getLongitude(), 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (addresses != null) {
                    parseCurrentPage("");
                }
            }
        }
    }

    private void parseCurrentPage(String startUrl) {
        String locality = TextUtils.isEmpty(addresses.get(0).getSubAdminArea()) ? addresses.get(0).getLocality() :
                addresses.get(0).getSubAdminArea();
        String URI = SHOWTIME_URI + startUrl + "?near=" + locality;
        Document doc = null;
        //get current location
        final ContentResolver resolver = getContentResolver();
        ArrayList<ContentProviderOperation> batch = new ArrayList<>();

        try {
            doc = Jsoup.connect(URI).get();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        Elements theaters = doc.select("div.theater");
        if (theaters.isEmpty()) {
            return;
        }

        for (Element t : theaters) {
            Theater theater = new Theater();
            ArrayList<BoxOfficeMovie> moviesInTheaterList = new ArrayList<>();

            theater.setName(t.getElementsByClass(THEATER_NAME).get(0).text());
            theater.setInfo(CinemaUtil.cinemaOutdatedIn(t.getElementsByClass(THEATER_INFO).get(0).text()));

            for (int j = 0; j < t.getElementsByClass(THEATER_MOVIE).size(); j++) {
                BoxOfficeMovie boxOfficeMovie = new BoxOfficeMovie();
                String title = t.getElementsByClass(THEATER_MOVIE)
                        .get(j).getElementsByClass("name").get(0).text();
                boxOfficeMovie.setNonStrippedTitle(title);
                title = stripOff3DEndPoint(title);

                boxOfficeMovie.setTitle(title);
                boxOfficeMovie.setDuration(t.getElementsByClass(THEATER_MOVIE).get(j)
                        .getElementsByClass("info").get(0).text());
                boxOfficeMovie.setShowTime(t.getElementsByClass(THEATER_MOVIE).get(j)
                        .getElementsByClass("times").get(0).text());
                moviesInTheaterList.add(boxOfficeMovie);
            }
            theater.setMoviesShown(moviesInTheaterList);
            ContentProviderOperation.Builder builder = ContentProviderOperation.newInsert(NewsFeedContract.TheaterDB.CONTENT_URI);
            builder.withValues(Theater.getContentValuesForModel(theater));
            batch.add(builder.build());
        }
        try {
            resolver.applyBatch(NewsFeedContract.CONTENT_AUTHORITY, batch);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }
    }

    private String stripOff3DEndPoint(String title) {
        title = title.lastIndexOf(",") > 0 ? title.substring(0, title.lastIndexOf(",")) :
                title;
        title = title.endsWith(" 3D") ? title.substring(0, title.lastIndexOf(" 3D")) :
                title;
        return title;
    }
}
