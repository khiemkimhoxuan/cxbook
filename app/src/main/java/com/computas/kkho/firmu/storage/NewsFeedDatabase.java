package com.computas.kkho.firmu.storage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by kkh on 05.03.2015.
 */
public class NewsFeedDatabase extends SQLiteOpenHelper {
    public static final String DB_NAME = "firmu_database";

    // NOTE: carefully update onUpgrade() when bumping database versions to make
    // sure user data is saved.
    private static final int VER_LAUNCH = 2;
    private static final int DB_VERSION = VER_LAUNCH;

    public NewsFeedDatabase(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + NewsFeedContract.TABLE_MOVIE + " ("
                + NewsFeedContract.MovieDB._ID + " integer primary key autoincrement,"
                + NewsFeedContract.MovieDB.ID + " text not null,"
                + NewsFeedContract.MovieDB.TITLE + " text not null)");
        db.execSQL("CREATE TABLE IF NOT EXISTS " + NewsFeedContract.TABLE_THEATERS + " ("
                + NewsFeedContract.TheaterDB._ID + " integer primary key autoincrement,"
                + NewsFeedContract.TheaterDB.NAME + " text not null unique, "
                + NewsFeedContract.TheaterDB.INFO + " text, "
                + NewsFeedContract.TheaterDB.MOVIES + " text, "
                + NewsFeedContract.TheaterDB.MOVIESTIME + " text)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + NewsFeedContract.TABLE_MOVIE);
        db.execSQL("DROP TABLE IF EXISTS " + NewsFeedContract.TABLE_THEATERS);
        onCreate(db);

    }
}
