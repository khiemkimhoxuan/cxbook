package com.computas.kkho.firmu.model;

import java.util.List;

/**
 * Created by kkh on 26.03.2015.
 */
public class DiscoverWrapper {
    private String id;
    private String page;
    private List<BoxOfficeMovie> results;
    private Number total_pages;
    private Number total_results;

    public String getId() {

        return id;
    }

    public String getPage() {

        return page;
    }

    public List<BoxOfficeMovie> getResults() {

        return results;
    }

    public Number getTotal_pages() {

        return total_pages;
    }

    public Number getTotal_results() {

        return total_results;
    }
}
