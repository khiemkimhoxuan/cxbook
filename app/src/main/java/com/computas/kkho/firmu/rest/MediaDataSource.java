package com.computas.kkho.firmu.rest;

/**
 * Created by kkh on 26.03.2015.
 */
public interface MediaDataSource {

    public void getShows();

    public void getMovies();

    public void getNowPlayingMovies();

    public void getSearchedMovie(String query);

    public void getDetailMovie(String id);

    public void getMovieTrailer(String id);

    public void getReviews(String id);

    public void getConfiguration();

    public void getImages(String movieId);

    public void getCredits(String movieId);

}
