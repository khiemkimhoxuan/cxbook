package com.computas.kkho.firmu.domain.controllers;


import android.content.Context;

import com.computas.kkho.firmu.domain.ConfigurationUsecase;
import com.computas.kkho.firmu.model.ConfigurationResponse;
import com.computas.kkho.firmu.rest.MediaDataSource;
import com.computas.kkho.firmu.util.Constants;
import com.computas.kkho.util.BusProvider;
import com.google.gson.Gson;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

/**
 * Created by kkh on 26.03.2015.
 */
public class ConfigurationUseCaseController implements ConfigurationUsecase {
    private final MediaDataSource mMediaDataSource;
    private final Bus mUIBus;
    private Context mContext;
    private Gson gson;

    public ConfigurationUseCaseController(Context context, MediaDataSource mediaDataSource, Bus uiBus) {
        mMediaDataSource = mediaDataSource;
        mUIBus = uiBus;
        mContext = context;
        gson = new Gson();
        BusProvider.getRestBusInstance().register(this);
    }


    @Override
    public void requestConfiguration() {
        ConfigurationResponse configurationResponse = gson.fromJson(mContext.getSharedPreferences(
                Constants.SHARED_PREFERENCE,
                Context.MODE_PRIVATE).
                getString(Constants.CONFIGURATION_SETUP, null), ConfigurationResponse.class);
        if (configurationResponse == null) {
            mMediaDataSource.getConfiguration();
        } else {
            onConfigurationReceived(configurationResponse);
        }

    }

    @Subscribe
    @Override
    public void onConfigurationReceived(ConfigurationResponse configurationResponse) {
        BusProvider.getRestBusInstance().unregister(this);
        configureImageUrl(configurationResponse);
        mContext.getSharedPreferences(Constants.SHARED_PREFERENCE, Context.MODE_PRIVATE)
                .edit()
                .putString(Constants.CONFIGURATION_SETUP, gson.toJson(configurationResponse))
                .commit();
    }

    @Override
    public void configureImageUrl(ConfigurationResponse configurationResponse) {
        String url = "";

        if (configurationResponse.getImages() != null) {
            url = configurationResponse.getImages()
                    .getBase_url();
            String imageQuality = "";

            for (String quality : configurationResponse.getImages().getPoster_sizes()) {
                if (quality.equals(Constants.PREFERRED_QUALITY)) {
                    imageQuality = Constants.PREFERRED_QUALITY;
                    break;
                }
            }

            if (imageQuality.equals("")) {
                imageQuality = "original";
            }

            url += imageQuality;
            sendConfiguredUrlToPresenter(url);
        }
    }

    @Override
    public void sendConfiguredUrlToPresenter(String url) {
        mUIBus.post(url);
    }

    @Override
    public void execute() {
        requestConfiguration();
    }

    public void stop() {
        mUIBus.unregister(this);
    }
}
