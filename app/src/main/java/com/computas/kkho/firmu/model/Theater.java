package com.computas.kkho.firmu.model;

import android.content.ContentValues;

import com.computas.kkho.firmu.storage.NewsFeedContract;
import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by kkh on 19.03.2015.
 */
public class Theater implements Serializable {
    private String name;
    private String info;
    private String moviesToShow;
    private String movieTimeInfoShow;
    private ArrayList<BoxOfficeMovie> moviesShown;


    //locations will not be stored in the database.
    private LatLng latlng;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getMoviesShown() {
        StringBuilder sBuilder = new StringBuilder();
        for (BoxOfficeMovie movie : moviesShown) {
            sBuilder.append(movie.getTitle() + ";");
        }
        sBuilder.setLength(sBuilder.length() - 1);
        return sBuilder.toString();
    }

    public ArrayList<TheaterMovie> createTheaterMovieList() {
        ArrayList<TheaterMovie> theaterMovieList = new ArrayList<>();
        String splitEachIndividualMovie[] = getMovieTimeInfoString().split(";");
        for (String movie : splitEachIndividualMovie) {
            String movieInfo[] = movie.split("\\#Movie:|\\#Time:");
            theaterMovieList.add(new TheaterMovie(movieInfo[1], movieInfo[2]));
        }
        return theaterMovieList;
    }

    public void setMoviesShown(ArrayList<BoxOfficeMovie> moviesShown) {
        this.moviesShown = moviesShown;
    }

    public String getMoviesToShow() {
        return moviesToShow;
    }

    public void setMoviesToShow(String moviesToShow) {
        this.moviesToShow = moviesToShow;
    }

    public LatLng getLatlng() {
        return latlng;
    }

    public void setLatlng(LatLng latlng) {
        this.latlng = latlng;
    }

    public String getMovieTimeInfo() {
        StringBuilder sBuilder = new StringBuilder();
        for (BoxOfficeMovie movie : moviesShown) {
            sBuilder.append("#Movie:" + movie.getNonStripptedTitle() + "#Time:" + movie.getShowTime() + ";");
        }
        sBuilder.setLength(sBuilder.length() - 1);
        movieTimeInfoShow = sBuilder.toString();
        return sBuilder.toString();
    }

    public String getMovieTimeInfoString() {
        return movieTimeInfoShow;
    }

    public void setMovieShowTimeInfo(String movieShowTimeInfo) {
        this.movieTimeInfoShow = movieShowTimeInfo;
    }


    public static ContentValues getContentValuesForModel(Theater theater) {
        ContentValues values = new ContentValues(4);
        values.put(NewsFeedContract.TheaterDB.NAME, theater.getName());
        values.put(NewsFeedContract.TheaterDB.INFO, theater.getInfo());
        values.put(NewsFeedContract.TheaterDB.MOVIES, theater.getMoviesShown());
        values.put(NewsFeedContract.TheaterDB.MOVIESTIME, theater.getMovieTimeInfo());
        return values;
    }

    public static ContentValues updateContentValuesForModel(Theater theater) {
        ContentValues values = new ContentValues(1);
        values.put(NewsFeedContract.TheaterDB.MOVIES, theater.getMoviesShown());
        return values;
    }
}
