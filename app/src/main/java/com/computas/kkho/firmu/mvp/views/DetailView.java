package com.computas.kkho.firmu.mvp.views;

import android.graphics.Bitmap;

import com.computas.kkho.firmu.model.Credits;
import com.computas.kkho.firmu.model.MovieTrailer;
import com.computas.kkho.firmu.model.Review;

import java.util.List;

/**
 * Created by kkh on 26.03.2015.
 */
public interface DetailView {
    void showFilmCover(Bitmap bitmap);

    public void setName(String title);

    public void setDescription(String description);

    public void setHomepage(String homepage);

    public void setCompanies(String companies);

    public void setAverageRating(String rating);

    public void setTagline(String tagline);

    public void showMovieTrailer(MovieTrailer movieTrailer);

    public void finish(String cause);

    public void showConfirmationView();

    public void animateConfirmationView();

    public void startClosingConfirmationView();

    public void showReviews(List<Review> results);

    void showMovieImage(String url);

    public void showCredits(List<Credits.Cast> results);
}
