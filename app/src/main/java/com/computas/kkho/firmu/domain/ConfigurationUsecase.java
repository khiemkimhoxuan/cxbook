package com.computas.kkho.firmu.domain;

import com.computas.kkho.firmu.model.ConfigurationResponse;

/**
 * Created by kkh on 26.03.2015.
 */
public interface ConfigurationUsecase extends UseCase {

    public void requestConfiguration();

    public void onConfigurationReceived(ConfigurationResponse configurationResponse);

    public void configureImageUrl(ConfigurationResponse configurationResponse);

    public void sendConfiguredUrlToPresenter(String url);
}
