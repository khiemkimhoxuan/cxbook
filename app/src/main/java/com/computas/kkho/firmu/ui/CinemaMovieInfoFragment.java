package com.computas.kkho.firmu.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.computas.kkho.firmu.R;
import com.computas.kkho.firmu.adapter.MovieAdapter;
import com.computas.kkho.firmu.model.Theater;
import com.computas.kkho.firmu.model.TheaterMovie;
import com.computas.kkho.firmu.util.ColorUtil;
import com.computas.kkho.firmu.util.Constants;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by kkh on 18.04.2015.
 */
public class CinemaMovieInfoFragment extends Fragment {

    @InjectView(R.id.recycler_view_cinema_list)
    RecyclerView mMovieListRecyclerVIew;
    @InjectView(R.id.cinema_progressbar)
    ProgressBar mProgressBar;

    MovieAdapter mCinemaMovieAdapter;
    Theater mSelectedTheater;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle mArguments = this.getArguments();
        if (mArguments != null) {
            mSelectedTheater = (Theater) mArguments.getSerializable(Constants.MOVIE_PLAYING_CINEMA);
        }
        setRetainInstance(true);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cinema_recyclerview, container, false);
        ButterKnife.inject(this, view);
        ColorUtil.colorProgressBar(mProgressBar, getActivity());
        initRecyclerView(mSelectedTheater.createTheaterMovieList());
        return view;
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        if (mCinemaMovieAdapter != null) {
            mCinemaMovieAdapter.notifyItemRangeRemoved(0, mCinemaMovieAdapter.getItemCount());
            mCinemaMovieAdapter.getMovieList().clear();
            mCinemaMovieAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void initRecyclerView(ArrayList<TheaterMovie> movieList) {
        mCinemaMovieAdapter = new MovieAdapter(getActivity(), movieList, mProgressBar);
        mMovieListRecyclerVIew.setHasFixedSize(true);
        mMovieListRecyclerVIew.setAdapter(mCinemaMovieAdapter);
    }
}
