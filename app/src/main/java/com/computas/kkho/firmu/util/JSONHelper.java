package com.computas.kkho.firmu.util;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by kkh on 05.03.2015.
 */

public class JSONHelper {

    public static JSONObject ToJSONObject(String text) {
        JSONObject json;
        try {
            json = new JSONObject(text);
        } catch (JSONException e) {
            json = null;
        }
        return json;
    }
}
