package com.computas.kkho.firmu.ui;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.IntentSender;
import android.content.Loader;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.computas.kkho.firmu.Firmu;
import com.computas.kkho.firmu.R;
import com.computas.kkho.firmu.adapter.NewsFeedAdapter;
import com.computas.kkho.firmu.model.BoxOfficeMovie;
import com.computas.kkho.firmu.model.Theater;
import com.computas.kkho.firmu.mvp.presenters.MoviesPresenter;
import com.computas.kkho.firmu.mvp.views.MoviesView;
import com.computas.kkho.firmu.service.ShowTimeMovieService;
import com.computas.kkho.firmu.storage.NewsFeedContract;
import com.computas.kkho.firmu.util.CinemaUtil;
import com.computas.kkho.firmu.util.ColorUtil;
import com.computas.kkho.firmu.util.Constants;
import com.computas.kkho.firmu.util.NetworkUtil;
import com.computas.kkho.firmu.util.RecyclerViewClickListener;
import com.computas.kkho.firmu.util.UiUtils;
import com.computas.kkho.firmu.widget.AutofitRecyclerView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.squareup.leakcanary.RefWatcher;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by kkho on 17.01.2015.
 */
public class HomeFragment extends Fragment
        implements
        MoviesView,
        RecyclerViewClickListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        LoaderManager.LoaderCallbacks<Cursor> {

    public static final String BOX_OFFICE_MOVIE = "com.computas.BOX_OFFICE_MOVE";
    private boolean mStateHasRunShowTime = false;

    public static SparseArray<Bitmap> sPhotoCache = new SparseArray<>(1);
    private MoviesPresenter mMoviesPresenter;
    private ArrayList<Theater> mTheaters;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private static boolean hasLoadedContent = false;

    @InjectView(R.id.activity_main_swipe_refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @InjectView(R.id.recycler_view_movie_list)
    AutofitRecyclerView mNewsFeedRecyclerView;
    @InjectView(R.id.activity_newsfeed_progress)
    ProgressBar mProgressBar;
    @InjectView(R.id.failed_loading)
    RelativeLayout mShowErrorLayout;

    Snackbar mSnackBar;

    private static List<BoxOfficeMovie> mListItems = new ArrayList<>();
    NewsFeedAdapter mNewsFeedAdapter;

    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMoviesPresenter = new MoviesPresenter(getActivity(), this);
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(10 * 1000); // 1 second, in milliseconds

        clearCinemas();
        mStateHasRunShowTime = false;
        setRetainInstance(true);
        setHasOptionsMenu(true);
        getActivity().getLoaderManager().restartLoader(0, null, this);
    }

    private void initShowTimeIntentService() {
        Intent intent = new Intent(getActivity(), ShowTimeMovieService.class);
        intent.putExtra(FusedLocationProviderApi.KEY_LOCATION_CHANGED, mLocation);
        getActivity().startService(intent);
    }

    private void clearCinemas() {
        getActivity().getContentResolver().delete(NewsFeedContract.TheaterDB.CONTENT_URI, null, null);
    }

    @Override
    public void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.inject(this, view);
        initSwipeRefresh();
        showLoading();
        if (!NetworkUtil.isNetworkOn(getActivity())) {
            showError("");
            hideLoading();
        } else {
            if (!hasLoadedContent) {
                mMoviesPresenter.start();
                hasLoadedContent = true;
            } else {
                mMoviesPresenter.setMoviesView(this);
                initRecyclerView(mListItems);
            }
        }

        return view;
    }

    @Override
    public void onDetach() {
        getActivity().getContentResolver().unregisterContentObserver(mObserver);
        super.onDetach();
    }

    @Override
    public void onPause() {
        super.onPause();
        mStateHasRunShowTime = false;
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mStateHasRunShowTime = false;
        ButterKnife.reset(this);
        RefWatcher refWatcher = Firmu.getRefWatcher(getActivity());
        refWatcher.watch(this);
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
        sPhotoCache.clear();
        UiUtils.deleteDirectoryTree(getActivity().getCacheDir());
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().getContentResolver().registerContentObserver(
                NewsFeedContract.TheaterDB.CONTENT_URI, true, mObserver);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void showMovies(List<BoxOfficeMovie> movieList) {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
        initRecyclerView(movieList);
        hideLoading();

    }

    private void initRecyclerView(List<BoxOfficeMovie> movieList) {
        if (mNewsFeedRecyclerView != null) {
            mNewsFeedAdapter = new NewsFeedAdapter(getActivity(), movieList);
            mNewsFeedAdapter.setRecyclerListListener(this);
            mNewsFeedRecyclerView.setHasFixedSize(true);
            mListItems = movieList;
            mNewsFeedRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            mNewsFeedRecyclerView.setAdapter(mNewsFeedAdapter);
            mNewsFeedRecyclerView.addOnScrollListener(mRecyclerScrollListener);
        }
    }

    private void initSwipeRefresh() {
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.white);
        mSwipeRefreshLayout.setProgressBackgroundColorSchemeResource(R.color.theme_color);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();
            }
        });
    }

    // fake a network operation's delayed response
    // this is just for demonstration, not real code!
    private void refreshContent() {
        if (NetworkUtil.isNetworkOn(getActivity())) {
            hideError();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mNewsFeedAdapter != null) {
                        clearCinemaAdapter();
                    } else if (!hasLoadedContent) {
                        mMoviesPresenter.start();
                        hasLoadedContent = true;
                        showLoading();
                    }
                    clearCinemas();
                    mStateHasRunShowTime = false;
                    initShowTimeIntentService();
                    mListItems.clear();
                    mMoviesPresenter.refreshMovieList();
                }
            }, 2200);
        } else {
            if (mNewsFeedAdapter != null) {
                clearCinemaAdapter();
            }
            showError("");
            hideLoading();
            mSwipeRefreshLayout.setRefreshing(false);

        }
    }

    private void clearCinemaAdapter() {
        if (mNewsFeedAdapter != null) {
            int size = mNewsFeedAdapter.getItemCount();
            mMoviesPresenter.resetMovieList();
            mListItems.clear();
            mNewsFeedAdapter.getMovieList().clear();
            mNewsFeedAdapter.notifyItemRangeRemoved(0, size);
            mNewsFeedAdapter.notifyDataSetChanged();
        }
        hideLoading();
    }


    public void showLoading() {
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.VISIBLE);
            ColorUtil.colorProgressBar(mProgressBar, getActivity());
        }
    }

    public void hideLoading() {
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showError(String error) {
        mShowErrorLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideError() {
        mShowErrorLayout.setVisibility(View.GONE);
    }

    @Override
    public void showLoadingLabel() {
        mSnackBar = UiUtils.initSnackBar(getView(), getActivity());
        View view = mSnackBar.getView();
        view.setBackgroundColor(getResources().getColor(R.color.theme_color));
        mSnackBar.show();
    }

    @Override
    public void hideActionLabel() {
        if (mSnackBar != null) {
            //   mSnackBar.dismiss();
        }
    }

    @Override
    public boolean isTheListEmpty() {
        return (mNewsFeedAdapter == null) || mNewsFeedAdapter.getMovieList().isEmpty();
    }

    @Override
    public void appendMovies(List<BoxOfficeMovie> movieList) {
        mNewsFeedAdapter.appendMovies(movieList);
        if (mSwipeRefreshLayout != null) {
            hideLoading();
            mSwipeRefreshLayout.setRefreshing(false);
        }

    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLocation == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                    mLocationRequest, this);
        } else {
            onLocationChanged(mLocation);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_base, menu);
        UiUtils.createSearchWidget((AppCompatActivity) getActivity(), menu, null, mMoviesPresenter);
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                break;
        }
        return false;
    }

    private final ContentObserver mObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            if (getActivity() == null) {
                return;
            }
            Loader<Cursor> loader = getActivity().getLoaderManager().getLoader(0);
            if (loader != null) {
                loader.forceLoad();
            }
        }
    };

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(),
                NewsFeedContract.TheaterDB.CONTENT_URI,
                Constants.TheaterQuery.PROJECTION,
                null,
                null,
                null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (getActivity() == null) return;
        mTheaters = new ArrayList<>();
        while (data.moveToNext()) {
            Theater theater = new Theater();
            theater.setName(data.getString(Constants.TheaterQuery.NAME));
            theater.setInfo(data.getString(Constants.TheaterQuery.INFO));
            String moviesShown = data.getString(Constants.TheaterQuery.MOVIES);
            String movieInfoTime = data.getString(Constants.TheaterQuery.MOVIESTIME);
            theater.setMoviesToShow(moviesShown);
            theater.setMovieShowTimeInfo(movieInfoTime);
            mTheaters.add(theater);
        }
        mMoviesPresenter.setNewCinemas(CinemaUtil.allMoviesInCinema(mTheaters));
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    private RecyclerView.OnScrollListener mRecyclerScrollListener = new RecyclerView.OnScrollListener() {

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if (recyclerView == null || mNewsFeedRecyclerView.getLayoutManager() == null) {
                return;
            }

            super.onScrolled(recyclerView, dx, dy);

            int topRowVerticalPosition =
                    (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
            mSwipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);

            visibleItemCount = mNewsFeedRecyclerView.getLayoutManager().getChildCount();
            totalItemCount = mNewsFeedRecyclerView.getLayoutManager().getItemCount();
            pastVisiblesItems = ((GridLayoutManager) mNewsFeedRecyclerView.getLayoutManager())
                    .findFirstVisibleItemPosition();

            if ((visibleItemCount + pastVisiblesItems) >= totalItemCount && !mMoviesPresenter.isLoading()) {
                mMoviesPresenter.onEndListReached();
            }
        }
    };

    @Override
    public void onLocationChanged(Location location) {
        mLocation = location;
        if (!mStateHasRunShowTime && NetworkUtil.isNetworkOn(getActivity())) {
            clearCinemas();
            initShowTimeIntentService();
            mStateHasRunShowTime = true;
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(getActivity(), 9000);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            //Log.i("Out", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onClick(View v, int position, float x, float y) {
        Gson gson = new Gson();
        BoxOfficeMovie movie = mNewsFeedAdapter.getMovieList().get(position);
        ImageView movieCover = (ImageView) v.findViewById(R.id.ivPosterImage);
        Intent intent = new Intent(getActivity(), ItemMovieActivity.class);
        intent.putExtra(BOX_OFFICE_MOVIE, gson.toJson(movie));
        BitmapDrawable mBitMapDrawable = (BitmapDrawable) movieCover
                .getDrawable();
        if (movie.isMovieReady() && mBitMapDrawable != null) {
            sPhotoCache.put(0, mBitMapDrawable.getBitmap());
            UiUtils.startTransitionSharedElement(v, getActivity(), position,
                    intent);
        } else {
            Toast.makeText(getActivity(),
                    "Movie is loading, please wait...", Toast.LENGTH_SHORT).show();
        }
    }

    private interface MovieQuery {
        String[] PROJECTION = {
                NewsFeedContract.MovieDB.ID,
                NewsFeedContract.MovieDB.TITLE,
        };
        int ID = 0;
        int TITLE = 1;
    }
}
