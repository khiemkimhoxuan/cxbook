package com.computas.kkho.firmu.ui;

import android.support.v4.app.Fragment;

/**
 * Created by kkho on 17.01.2015.
 */
public class HomeActivity extends BaseSupportActivity {

    @Override
    public Fragment onCreatePane() {
        return new HomeFragment();
    }
}
