package com.computas.kkho.firmu.domain.controllers;

import com.computas.kkho.firmu.domain.GetMovieDetailUseCase;
import com.computas.kkho.firmu.model.Credits;
import com.computas.kkho.firmu.model.MovieDetail;
import com.computas.kkho.firmu.model.MovieImageWrapper;
import com.computas.kkho.firmu.model.MovieTrailer;
import com.computas.kkho.firmu.model.ReviewWrapper;
import com.computas.kkho.firmu.rest.MediaDataSource;
import com.computas.kkho.util.BusProvider;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

/**
 * Created by kkh on 26.03.2015.
 */
public class GetMovieDetailUseCaseController implements GetMovieDetailUseCase {
    private final MediaDataSource mMovieDataSource;
    private final String mMovieId;
    private final Bus mUiBus;
    private MovieDetail mMovieDetail;

    public GetMovieDetailUseCaseController(String movieId,
                                           Bus uiBus, MediaDataSource dataSource) {
        mMovieId = movieId;
        mUiBus = uiBus;
        mMovieDataSource = dataSource;
        BusProvider.getRestBusInstance().register(this);
    }


    @Override
    public void requestMovieDetail(String movieId) {
        mMovieDataSource.getDetailMovie(movieId);
    }

    @Override
    public void requestMovieTrailer(String movieId) {
        mMovieDataSource.getMovieTrailer(movieId);
    }

    @Override
    public void requestMovieReviews(String movieId) {
        mMovieDataSource.getReviews(movieId);
    }

    @Override
    public void requestMovieImages(String movieId) {
        mMovieDataSource.getImages(movieId);
    }

    @Override
    public void requestCreditsResponse(String movieId) {
        mMovieDataSource.getCredits(movieId);
    }

    @Subscribe
    @Override
    public void onMovieDetailResponse(MovieDetail response) {
        mMovieDetail = response;
        requestCreditsResponse(mMovieId);

    }

    @Subscribe
    public void onMovieTrailerReceived(MovieTrailer response) {
        mUiBus.post(response);
        BusProvider.getRestBusInstance().unregister(this);
    }

    @Subscribe
    @Override
    public void onMovieReviewsResponse(ReviewWrapper reviewsWrapper) {
        sendDetailMovieToPresenter(mMovieDetail);
        mUiBus.post(reviewsWrapper);
        BusProvider.getRestBusInstance().unregister(this);
    }

    @Override
    public void onMovieImagesResponse(MovieImageWrapper imageWrapper) {
        //	mMovieDetail.setMovieImagesList(imageWrapper.getBackdrops());
        requestMovieReviews(mMovieId);
    }

    @Subscribe
    @Override
    public void onMovieCreditsResponse(Credits credits) {
        sendDetailMovieToPresenter(mMovieDetail);
        mUiBus.post(credits);
        requestMovieTrailer(mMovieId);
    }

    @Override
    public void sendDetailMovieToPresenter(MovieDetail response) {
        mUiBus.post(response);
    }

    @Override
    public void execute() {
        requestMovieDetail(mMovieId);
    }
}
