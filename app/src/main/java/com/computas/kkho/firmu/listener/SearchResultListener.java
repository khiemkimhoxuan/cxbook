package com.computas.kkho.firmu.listener;

import android.app.SearchManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.MenuItem;

import com.computas.kkho.firmu.adapter.NewsFeedAdapter;
import com.computas.kkho.firmu.model.BoxOfficeMovie;
import com.computas.kkho.firmu.mvp.presenters.MoviesSearchPresenter;
import com.computas.kkho.firmu.ui.SearchResultActivity;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by kkho on 06.06.2015.
 */
public class SearchResultListener implements android.widget.SearchView.OnQueryTextListener {
    AppCompatActivity mActivity;
    List<BoxOfficeMovie> mMovieList;
    SearchView mSearchView;
    MenuItem mSearchItem;
    WeakReference<MoviesSearchPresenter> mMovieSearchPresenter;

    public SearchResultListener(AppCompatActivity activity, MoviesSearchPresenter moviesSearchPresenter,
                                List<BoxOfficeMovie> movieList, NewsFeedAdapter adapter, SearchView searchView,
                                MenuItem searchItem) {
        mActivity = activity;

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Intent intent = new Intent(mActivity, SearchResultActivity.class);
        intent.putExtra(SearchManager.QUERY, query);
        /*
        if (mActivity instanceof HomeActivity) {
            mActivity.startActivity(intent);
        } else {
            moviesSearchPresenter.refreshMovieList();
            moviesSearchPresenter.setQuery(query);
            moviesSearchPresenter.beginSearch(query);
        }

        if (newsFeedAdapter != null && movieList != null) {
            int size = movieList.size();
            movieList.clear();
            newsFeedAdapter.notifyItemRangeRemoved(0, size);
            newsFeedAdapter.notifyDataSetChanged();
        }

        mSearchView.setIconified(true);
        mSearchView.clearFocus();
        mSearchItem.collapseActionView();*/
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        // TODO Auto-generated method stub
        return false;
    }
}
