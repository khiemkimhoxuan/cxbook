package com.computas.kkho.firmu.mvp.presenters;

import android.content.Context;
import android.text.TextUtils;

import com.computas.kkho.firmu.adapter.NewsFeedAdapter;
import com.computas.kkho.firmu.domain.controllers.ConfigurationUseCaseController;
import com.computas.kkho.firmu.domain.controllers.GetMoviesUsecaseController;
import com.computas.kkho.firmu.model.BoxOfficeMovie;
import com.computas.kkho.firmu.model.BoxOfficeSearchMovieWrapper;
import com.computas.kkho.firmu.mvp.views.MoviesView;
import com.computas.kkho.firmu.rest.RestMovieSource;
import com.computas.kkho.util.BusProvider;
import com.computas.kkho.util.MovieDbApiUrlInfo;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kkho on 18.05.2015.
 */
public class MoviesSearchPresenter extends Presenter {


    private MoviesView mMoviesView;
    private GetMoviesUsecaseController mGetPopularShows;
    private ConfigurationUseCaseController mConfigureUsecase;
    private static ArrayList<String> mMoviesInCinema = new ArrayList<>();
    private NewsFeedAdapter mNewsFeedAdapter;
    private List<BoxOfficeMovie> mListItems;


    private boolean isLoading = false;
    private String mQuery;

    public MoviesSearchPresenter(Context context, MoviesView moviesView, NewsFeedAdapter newsFeedAdapter,
                                 List<BoxOfficeMovie> listItems) {
        mMoviesView = moviesView;
        mGetPopularShows = MoviesPresenter.mGetPopularShows;

        mConfigureUsecase = new ConfigurationUseCaseController(context,
                RestMovieSource.getInstance(), BusProvider.getUIBusInstance());
        mNewsFeedAdapter = newsFeedAdapter;
        mListItems = listItems;
    }

    @Subscribe
    public void onSearchrMoviesReceived(BoxOfficeSearchMovieWrapper moviesWrapper) {

        if (mMoviesView.isTheListEmpty()) {
            mMoviesView.hideLoading();
            mMoviesView.showMovies(moviesWrapper.getResults());
        } else {
            mMoviesView.hideActionLabel();
            mMoviesView.hideLoading();
            mMoviesView.appendMovies(moviesWrapper.getResults());
        }
        isLoading = false;
    }

    @Subscribe
    public void onConfigurationFinished(String baseImageUrl) {
        MovieDbApiUrlInfo.BASIC_STATIC_URL = baseImageUrl;
        if (!TextUtils.isEmpty(mQuery)) {
            mGetPopularShows.requestSearchMovies(mQuery);
        }
    }

    public void onEndListReached() {
        if (mGetPopularShows.requestSearchMovies(mQuery)) {
            mMoviesView.showLoadingLabel();
            isLoading = true;
        }
    }

    public void beginSearch(String query) {
        if (mGetPopularShows.requestSearchMovies(mQuery)) {
            mMoviesView.showLoadingLabel();
            isLoading = true;
        }
    }

    public void refreshMovieList() {
        mGetPopularShows.executeSearchRefresh();
        isLoading = false;
    }

    public void setMoviesView(MoviesView moviesView) {
        mMoviesView = moviesView;
    }


    @Override
    public void start() {

        if (mMoviesView.isTheListEmpty()) {
            BusProvider.getUIBusInstance().register(this);
            if (!TextUtils.isEmpty(mQuery)) {
                mGetPopularShows.requestSearchMovies(mQuery);
            }
        }
    }

    public void setQuery(String query) {
        mQuery = query;
    }


    @Override
    public void stop() {
        BusProvider.getUIBusInstance().unregister(this);
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean isLoading) {
        this.isLoading = isLoading;
    }

    public void removeCinemaMovie(String movieTitle) {

    }

    public void addMovieFromCinema(String original_title) {
        mMoviesInCinema.add(original_title);
    }

}
