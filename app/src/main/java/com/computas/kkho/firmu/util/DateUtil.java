package com.computas.kkho.firmu.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by kkh on 18.04.2015.
 */
public class DateUtil {
    private static Calendar calendar;

    public static int getDateInstance() {
        if (calendar == null) {
            calendar = new GregorianCalendar();
        }
        return calendar.get(Calendar.YEAR);
    }

    public static Date getTimeNow() {
        if (calendar == null) {
            calendar = new GregorianCalendar();
        }
        return calendar.getTime();
    }

    public static Date convertStringToDate(String dateStr) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        return simpleDateFormat.parse(dateStr);
    }

    public static String getTimeInfos(String timeInfo) throws ParseException {
        String timeInformation[] = timeInfo.split(" ");
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        String timeNow = dateFormat.format(DateUtil.getTimeNow());
        StringBuilder stringBuilder = new StringBuilder();
        for (String timeSchedule : timeInformation) {
            if (DateUtil.convertStringToDate(timeSchedule).compareTo(
                    DateUtil.convertStringToDate(timeNow)) >= 0) {
                stringBuilder.append(timeSchedule + " ");
            }
        }
        return stringBuilder.toString();
    }

    public static String formatYearMonthDayDate(String release_date) {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        SimpleDateFormat currentFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return format.format(currentFormat.parse(release_date));
        } catch (ParseException pe) {

        }
        return "";
    }
}
