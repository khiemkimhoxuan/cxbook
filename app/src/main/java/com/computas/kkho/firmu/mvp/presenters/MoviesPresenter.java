package com.computas.kkho.firmu.mvp.presenters;

import android.content.Context;

import com.computas.kkho.firmu.domain.controllers.ConfigurationUseCaseController;
import com.computas.kkho.firmu.domain.controllers.GetMoviesUsecaseController;
import com.computas.kkho.firmu.model.BoxOfficeMovieWrapper;
import com.computas.kkho.firmu.model.BoxOfficeMovieWrapperNowPlaying;
import com.computas.kkho.firmu.model.DiscoverWrapper;
import com.computas.kkho.firmu.mvp.views.MoviesView;
import com.computas.kkho.firmu.rest.RestMovieSource;
import com.computas.kkho.util.BusProvider;
import com.computas.kkho.util.MovieDbApiUrlInfo;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;


/**
 * Created by kkh on 26.03.2015.
 */
public class MoviesPresenter extends Presenter {

    private MoviesView mMoviesView;
    public static GetMoviesUsecaseController mGetPopularShows;
    private ConfigurationUseCaseController mConfigureUsecase;
    private static ArrayList<String> mMoviesInCinema = new ArrayList<>();
    private boolean isLoading = false;
    private static boolean isRegistered = false;


    public MoviesPresenter(Context context, MoviesView moviesView) {

        mMoviesView = moviesView;
        mGetPopularShows = new GetMoviesUsecaseController(context,
                RestMovieSource.getInstance(), BusProvider.getUIBusInstance());

        mConfigureUsecase = new ConfigurationUseCaseController(context,
                RestMovieSource.getInstance(), BusProvider.getUIBusInstance());
    }

    @Subscribe
    public void onPopularMoviesReceived(BoxOfficeMovieWrapper moviesWrapper) {
        if (moviesWrapper != null) {

            if (mMoviesView.isTheListEmpty()) {
                mMoviesView.hideLoading();
                mMoviesView.showMovies(moviesWrapper.getResults());
            } else {
                mMoviesView.hideActionLabel();
                mMoviesView.hideLoading();
                mMoviesView.appendMovies(moviesWrapper.getResults());
            }
        }
        isLoading = false;
    }

    @Subscribe
    public void onNowPlayingMoviesReceived(BoxOfficeMovieWrapperNowPlaying moviesWrapper) {
        if (mMoviesView.isTheListEmpty()) {
            mMoviesView.hideLoading();
            mMoviesView.showMovies(moviesWrapper.getResults());
        } else {
            mMoviesView.hideActionLabel();
            mMoviesView.hideLoading();
            mMoviesView.appendMovies(moviesWrapper.getResults());
        }
        isLoading = false;
    }

    @Subscribe
    public void onDiscoverMoviesReceived(DiscoverWrapper discoverWrapper) {
        if (mMoviesView.isTheListEmpty()) {
            mMoviesView.hideLoading();
            mMoviesView.showMovies(discoverWrapper.getResults());
        } else {
            mMoviesView.hideActionLabel();
            mMoviesView.hideLoading();
            mMoviesView.appendMovies(discoverWrapper.getResults());
        }
        isLoading = false;
    }

    @Subscribe
    public void onConfigurationFinished(String baseImageUrl) {
        MovieDbApiUrlInfo.BASIC_STATIC_URL = baseImageUrl;
        mGetPopularShows.execute();
    }

    public void onEndListReached() {
        mGetPopularShows.execute();
        mMoviesView.showLoadingLabel();
        isLoading = true;
    }

    public void refreshMovieList() {
        mGetPopularShows.executeRefresh();
        isLoading = true;
    }

    public void resetMovieList() {
        mGetPopularShows.resetRequestPage();
        isLoading = false;
    }

    public void setNewCinemas(ArrayList<String> movies) {
        //	BusProvider.getUIBusInstance().register(this);
        //	mRegistered = true;
        //	mConfigureUsecase.execute();
        //	mGetPopularShows.addSearchedMovies(movies);
        //	isLoading = true;
    }

    public void setMoviesView(MoviesView moviesView) {
        mMoviesView = moviesView;
    }


    @Override
    public void start() {
        if (mMoviesView.isTheListEmpty()) {
            BusProvider.getUIBusInstance().register(this);
            mConfigureUsecase.execute();
        }
    }


    @Override
    public void stop() {
        mGetPopularShows.unRegister();
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean isLoading) {
        this.isLoading = isLoading;
    }

    public void removeCinemaMovie(String movieTitle) {

    }

    public void addMovieFromCinema(String original_title) {
        mMoviesInCinema.add(original_title);
    }
}