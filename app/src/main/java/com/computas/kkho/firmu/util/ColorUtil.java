package com.computas.kkho.firmu.util;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.computas.kkho.firmu.R;
import com.melnykov.fab.FloatingActionButton;

/**
 * Created by kkh on 01.05.2015.
 */
public class ColorUtil {

    // A method to find height of the status bar
    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static void setActionbarColor(int titleColor, Toolbar actionbar) {
        actionbar.setTitle(TextUtil.setSpannableText(titleColor, actionbar.getTitle()));
    }

    public static int getThemeAccentColor(final Context context) {
        final TypedValue value = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.colorPrimary, value, true);
        return value.data;
    }

    public static void clearColorTextView(RecyclerView recyclerView, Context context) {
        for (int i = 0; i < recyclerView.getChildCount(); i++) {
            recyclerView.getChildAt(i).setBackgroundColor(context.getResources().getColor(R.color.theme_color));
        }
    }

    public static void changeTextColorListView(ListView listView, int color) {
        for (int i = 0; i < listView.getChildCount(); i++) {
            ((TextView) listView.getChildAt(i)).setTextColor(color);
        }
    }

    public static void fabButtonColorFilterIcon(FloatingActionButton fabbutton, int color) {

    }

    public static void colorIcons(ImageView imageView, Context mContext, int color) {
        imageView.setColorFilter(mContext.getResources().getColor(color), PorterDuff.Mode.SRC_IN);
    }

    public static void colorImage(ImageView imageView, Context mContext, int color) {
        imageView.setColorFilter(color, PorterDuff.Mode.SRC_IN);
    }

    public static void colorProgressBar(ProgressBar mProgressBar, Context mContext) {
        mProgressBar.getIndeterminateDrawable().setColorFilter(mContext.getResources().getColor(R.color.theme_color),
                PorterDuff.Mode.MULTIPLY);
    }
}
