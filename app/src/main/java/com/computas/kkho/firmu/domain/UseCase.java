package com.computas.kkho.firmu.domain;

/**
 * Created by kkh on 26.03.2015.
 */
public interface UseCase {
    public void execute();
}
