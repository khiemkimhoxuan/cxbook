package com.computas.kkho.firmu.util;

import android.content.Context;
import android.database.Cursor;
import android.location.Geocoder;
import android.location.Location;

import com.computas.kkho.firmu.storage.NewsFeedContract;
import com.google.android.gms.maps.model.LatLng;

import java.util.Locale;

/**
 * Created by kkh on 17.03.2015.
 */
public class LocationUtil {
    private static final int FORTY_KILOMETERS = 40000;

    public static boolean isDistanceFortyKilometersFromPosition(Location from, Location to) {
        return (int) locationCalc(from, to) <= FORTY_KILOMETERS;
    }

    private static float locationCalc(Location from, Location to) {
        return from.distanceTo(to);
    }

    public static LatLng getCoordinatesFromAddress() {
        return null;
    }

    public static Location locationFactory(LatLng latLng) {
        Location location = new Location("");
        location.setLatitude(latLng.latitude);
        location.setLongitude(latLng.longitude);
        return location;
    }

    public static Geocoder geocoderFactory(Context context) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        return geocoder;
    }

    public static int countCinemaDbStored(Context context) {
        Cursor cursor = context.getContentResolver().query(NewsFeedContract.TheaterDB.CONTENT_URI,
                null, null, null, null);
        cursor.moveToFirst();
        return cursor.getCount();
    }
}
