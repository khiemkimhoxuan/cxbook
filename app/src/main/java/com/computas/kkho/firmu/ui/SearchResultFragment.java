package com.computas.kkho.firmu.ui;

import android.app.SearchManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.computas.kkho.firmu.Firmu;
import com.computas.kkho.firmu.R;
import com.computas.kkho.firmu.adapter.NewsFeedAdapter;
import com.computas.kkho.firmu.model.BoxOfficeMovie;
import com.computas.kkho.firmu.mvp.presenters.MoviesSearchPresenter;
import com.computas.kkho.firmu.mvp.views.MoviesView;
import com.computas.kkho.firmu.util.ColorUtil;
import com.computas.kkho.firmu.util.RecyclerViewClickListener;
import com.computas.kkho.firmu.util.UiUtils;
import com.computas.kkho.firmu.widget.AutofitRecyclerView;
import com.google.gson.Gson;
import com.squareup.leakcanary.RefWatcher;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by kkh on 17.05.2015.
 */
public class SearchResultFragment extends Fragment implements
        MoviesView, RecyclerViewClickListener {

    private String mQueryString;
    public static SparseArray<Bitmap> sPhotoCache = new SparseArray<>(1);
    private MoviesSearchPresenter mMoviesPresenter;

    public float mBackgroundTranslation;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private static boolean hasLoadedContent = false;
    @InjectView(R.id.recycler_view_movie_list)
    AutofitRecyclerView mNewsFeedRecyclerView;
    @InjectView(R.id.activity_newsfeed_progress)
    ProgressBar mProgressBar;
    @InjectView(R.id.failed_loading)
    RelativeLayout mFailedSearchLayout;

    Snackbar mSnackBar;

    List<BoxOfficeMovie> mListItems = new ArrayList<>();
    NewsFeedAdapter mNewsFeedAdapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMoviesPresenter = new MoviesSearchPresenter(getActivity(), this, mNewsFeedAdapter, mListItems);
        mMoviesPresenter.refreshMovieList();
        setRetainInstance(true);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_search, container, false);
        mQueryString = this.getArguments().getString(SearchManager.QUERY);
        mMoviesPresenter.setQuery(mQueryString);
        ButterKnife.inject(this, view);
        mMoviesPresenter.start();
        showLoading();
        return view;
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        RefWatcher refWatcher = Firmu.getRefWatcher(getActivity());
        refWatcher.watch(this);
        sPhotoCache.clear();
        mMoviesPresenter.refreshMovieList();
        if (mNewsFeedAdapter != null) {
            mNewsFeedAdapter.getMovieList().clear();
            int size = mListItems.size();
            mListItems.clear();
            mNewsFeedAdapter.notifyItemRangeRemoved(0, size);
            mNewsFeedAdapter.notifyDataSetChanged();
        }
        UiUtils.deleteDirectoryTree(getActivity().getCacheDir());
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_base, menu);
        UiUtils.createSearchWidget((AppCompatActivity) getActivity(), menu, mMoviesPresenter, null);
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                if (mNewsFeedAdapter != null) {
                    int size = mListItems.size();
                    mListItems.clear();
                    mNewsFeedAdapter.getMovieList().clear();
                    mNewsFeedAdapter.notifyItemRangeRemoved(0, size);
                    mNewsFeedAdapter.notifyDataSetChanged();
                }
                break;
        }
        return false;
    }


    @Override
    public void showMovies(List<BoxOfficeMovie> movieList) {
        initRecyclerView(movieList);
        hideLoading();

    }


    private void initRecyclerView(List<BoxOfficeMovie> movieList) {
        if (!movieList.isEmpty() && mNewsFeedRecyclerView != null) {
            mListItems = movieList;
            mNewsFeedAdapter = new NewsFeedAdapter(getActivity(), movieList);
            mNewsFeedAdapter.setRecyclerListListener(this);
            mNewsFeedRecyclerView.setHasFixedSize(true);
            mNewsFeedRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            mNewsFeedRecyclerView.setAdapter(mNewsFeedAdapter);
            mNewsFeedRecyclerView.addOnScrollListener(mRecyclerScrollListener);
        }
    }


    public void showLoading() {
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.VISIBLE);
            ColorUtil.colorProgressBar(mProgressBar, getActivity());
        }
    }

    public void hideLoading() {
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showError(String error) {

    }

    @Override
    public void hideError() {

    }


    @Override
    public void showLoadingLabel() {
        mSnackBar = UiUtils.initSnackBar(getView(), getActivity());
        View view = mSnackBar.getView();
        view.setBackgroundColor(getResources().getColor(R.color.theme_color));
        mSnackBar.show();
    }

    @Override
    public void hideActionLabel() {
        if (mSnackBar != null) {
            //   mSnackBar.dismiss();
        }
    }

    @Override
    public boolean isTheListEmpty() {
        return (mNewsFeedAdapter == null) || mNewsFeedAdapter.getMovieList().isEmpty();
    }

    @Override
    public void appendMovies(List<BoxOfficeMovie> movieList) {
        mListItems.addAll(movieList);
        mNewsFeedAdapter.appendMovies(movieList);
        mNewsFeedAdapter.notifyDataSetChanged();
    }


    private RecyclerView.OnScrollListener mRecyclerScrollListener = new RecyclerView.OnScrollListener() {

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if (recyclerView == null || mNewsFeedRecyclerView.getLayoutManager() == null) {
                return;
            }

            super.onScrolled(recyclerView, dx, dy);

            visibleItemCount = mNewsFeedRecyclerView.getLayoutManager().getChildCount();
            totalItemCount = mNewsFeedRecyclerView.getLayoutManager().getItemCount();
            pastVisiblesItems = ((GridLayoutManager) mNewsFeedRecyclerView.getLayoutManager())
                    .findFirstVisibleItemPosition();

            if ((visibleItemCount + pastVisiblesItems) >= totalItemCount && !mMoviesPresenter.isLoading()) {
                mMoviesPresenter.onEndListReached();
            }
        }
    };

    @Override
    public void onClick(View v, int position, float x, float y) {
        Gson gson = new Gson();
        BoxOfficeMovie movie = mNewsFeedAdapter.getMovieList().get(position);
        ImageView movieCover = (ImageView) v.findViewById(R.id.ivPosterImage);
        Intent intent = new Intent(getActivity(), ItemMovieActivity.class);
        intent.putExtra(HomeFragment.BOX_OFFICE_MOVIE, gson.toJson(movie));
        BitmapDrawable mBitMapDrawable = (BitmapDrawable) movieCover
                .getDrawable();
        if (movie.isMovieReady() && mBitMapDrawable != null) {
            sPhotoCache.put(0, mBitMapDrawable.getBitmap());
            HomeFragment.sPhotoCache.clear();
            UiUtils.startTransitionSharedElement(v, getActivity(), position,
                    intent);
        } else {
            Toast.makeText(getActivity(),
                    "Movie is loading, please wait...", Toast.LENGTH_SHORT).show();
        }
    }
}
