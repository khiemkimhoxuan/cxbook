package com.computas.kkho.firmu.util;


import java.util.regex.Pattern;

public final class EmailPhoneNumberValidatorUtil {
    public static final String EMAIL_PHONE_KEY_VALUE = "newsletter_key";

    public static boolean regexValidation(String regexp, String value) {
        Pattern pattern = Pattern.compile(regexp);
        return pattern.matcher(value).matches();
    }

    public static boolean isEmailAddressValid(String emailaddress) {
        return regexValidation("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
                emailaddress);
    }

    public static boolean isMobileNumberValid(String number) {
        return regexValidation("\\d{8}?", number);
    }

    public static String regexPhoneNumber() {
        return " - .\\d.*";
    }
}
