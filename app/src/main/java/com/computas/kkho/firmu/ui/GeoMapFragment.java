package com.computas.kkho.firmu.ui;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.IntentSender;
import android.content.Loader;
import android.database.ContentObserver;
import android.database.Cursor;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.computas.kkho.firmu.Firmu;
import com.computas.kkho.firmu.R;
import com.computas.kkho.firmu.adapter.InfoWindowAdapterMarker;
import com.computas.kkho.firmu.model.BoxOfficeMovie;
import com.computas.kkho.firmu.model.Theater;
import com.computas.kkho.firmu.model.TheaterMovie;
import com.computas.kkho.firmu.service.FetchLocationFromAddressService;
import com.computas.kkho.firmu.service.ShowTimeMovieService;
import com.computas.kkho.firmu.storage.NewsFeedContract;
import com.computas.kkho.firmu.util.ColorUtil;
import com.computas.kkho.firmu.util.Constants;
import com.computas.kkho.firmu.util.LocationUtil;
import com.computas.kkho.firmu.util.TextUtil;
import com.computas.kkho.firmu.util.ToolbarHelper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.leakcanary.RefWatcher;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by kkh on 20.03.2015.
 */
public class GeoMapFragment extends Fragment implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        LoaderManager.LoaderCallbacks<Cursor> {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private Theater mCurrentTheater;
    private BoxOfficeMovie mMovieInCinema;
    private LatLng mLatLng;
    private LocationRequest mLocationRequest;
    private List<Theater> mCinemas;
    private List<TheaterMovie> mMovieTimeInfos;
    ArrayList<MarkerOptions> mMarkerForLocations;
    private AddressResultReceiver mResultReceiver;
    private int mTextColor = -1;
    private int mBackgroundColor = -1;
    private boolean mStateShowAllMovies = false;
    private boolean mStateFetchAddress = false;
    private MarkerOptions options;

    private InfoWindowAdapterMarker mInfoWindowAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int dialogColor = R.color.primary_dark;
        Bundle mArguments = this.getArguments();
        Toolbar mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);

        if (mArguments != null) {
            mTextColor = mArguments.getInt(Constants.TEXT_COLOR_SWATCH, -1);
            mBackgroundColor = mArguments.getInt(Constants.BACKGROUND_COLOR_SWATCH, -1);
            mStateShowAllMovies = mArguments.getBoolean(Constants.SHOW_ALL_CINEMAS, false);
            mCurrentTheater = (Theater) mArguments.getSerializable(Constants.CINEMA_LOCATION);

            if (mTextColor != -1 && mBackgroundColor != -1 && mToolbar != null) {
                mToolbar.setBackgroundColor(mBackgroundColor);
                ToolbarHelper.colorizeToolbar(mToolbar, mTextColor, getActivity());
                ColorUtil.setActionbarColor(mTextColor, mToolbar);
            }

            mMovieInCinema = (BoxOfficeMovie) mArguments.getSerializable(Constants.MOVIE_PLAYING_CINEMA);
        }


        mMarkerForLocations = new ArrayList<>();
        mResultReceiver = new AddressResultReceiver(new Handler());
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_LOW_POWER)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds

        if (Geocoder.isPresent()) {
            startIntentService();
        }
        setRetainInstance(true);
        setHasOptionsMenu(true);
        getActivity().getLoaderManager().restartLoader(0, null, this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map_layout, container, false);
        ButterKnife.inject(this, view);
        mInfoWindowAdapter = new InfoWindowAdapterMarker(getActivity(), mBackgroundColor);
        setUpMapIfNeeded();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpMapIfNeeded();
        mGoogleApiClient.connect();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        getActivity().getContentResolver().unregisterContentObserver(mObserver);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        RefWatcher refWatcher = Firmu.getRefWatcher(getActivity());
        refWatcher.watch(this);
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getActivity().getContentResolver().registerContentObserver(
                NewsFeedContract.TheaterDB.CONTENT_URI, true, mObserver);
    }


    private final ContentObserver mObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            if (getActivity() == null) {
                return;
            }
            Loader<Cursor> loader = getActivity().getLoaderManager().getLoader(0);
            if (loader != null) {
                loader.forceLoad();
            }
        }
    };

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = ((SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.map)).getMap();
            mMap.setInfoWindowAdapter(mInfoWindowAdapter);
            if (GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getActivity())
                    == ConnectionResult.SUCCESS) {
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                mMap.getUiSettings().setZoomControlsEnabled(true);
                mMap.getUiSettings().setCompassEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                mMap.getUiSettings().setAllGesturesEnabled(true);
            }
            // Check if we were successful in obtaining the map.
            if (mMap != null && mLocation != null
                    && !mCinemas.isEmpty()) {
                mMarkerForLocations.clear();
                setUpMap();
            }
        }
    }

    private void setUpMap() {
        //add markers to the location of movies around you
        mMap.clear();
        CameraUpdate cameraUpdate;
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        mLatLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
        //Setup service for getting address long and lat:
        //get address from service and
        if (mMarkerForLocations.size() > 1) {
            //loop through the markers for now just add a markeroption
            for (MarkerOptions markerOptions : mMarkerForLocations) {
                if (markerOptions.getPosition() != null) {
                    mMap.addMarker(markerOptions);
                }
                builder.include(markerOptions.getPosition());
            }
            LatLngBounds bounds = builder.build();
            cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 150);
            mMap.moveCamera(cameraUpdate);
        }
    }

    private void startIntentService() {
        Intent intent = new Intent(getActivity(), FetchLocationFromAddressService.class);
        intent.putExtra(Constants.RECEIVER, mResultReceiver);
        intent.putExtra(Constants.CINEMA_LOCATION_RECEIVER, mCurrentTheater);
        getActivity().startService(intent);
    }

    private void createCinemaMarkers() {
        if (mCurrentTheater != null) {
            if (mCurrentTheater.getLatlng() != null) {
                mMarkerForLocations.add(defineMarkerOptions(mCurrentTheater.getLatlng(), mCurrentTheater.getName(),
                        mCurrentTheater.getInfo(), null));
            }
        } else {

            for (Theater theater : mCinemas) {
                mMovieTimeInfos = theater.createTheaterMovieList();
                if (mStateShowAllMovies) {
                    mMarkerForLocations.add(defineMarkerOptions(theater.getLatlng(), theater.getName(),
                            theater.getInfo(), null));

                } else if (compareMovieInformation(theater.getMoviesToShow())) {
                    List<String> timeInfo = new ArrayList<>();
                    for (TheaterMovie movieTheater : mMovieTimeInfos) {
                        if (compareMovieInformation(movieTheater.getMovieName())) {
                            timeInfo.add(movieTheater.getShowTime());
                        }
                    }
                    mMarkerForLocations.add(defineMarkerOptions(theater.getLatlng(), theater.getName(),
                            theater.getInfo(), timeInfo));

                }

            }

            if (mMarkerForLocations.isEmpty() && (mCurrentTheater == null || mCurrentTheater.getLatlng() == null)) {
                Toast.makeText(getActivity(), "No Cinemas found or the movie isn't playing at the moment!", Toast.LENGTH_SHORT).show();
                if (mGoogleApiClient.isConnected()) {
                    LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                    mGoogleApiClient.disconnect();
                }
                getFragmentManager().popBackStackImmediate();
            }
        }
    }

    private boolean compareMovieInformation(String movieInTheater) {
        return (TextUtil.unaccent(movieInTheater).toLowerCase()
                .contains(TextUtil.unaccent(removeTheAtTheStart
                        (mMovieInCinema.getOriginal_title()).toLowerCase())) ||
                TextUtil.unaccent(movieInTheater).toLowerCase()
                        .contains(TextUtil.unaccent(removeTheAtTheStart(mMovieInCinema.getTitle())).toLowerCase())
                || compareAndSkipWhiteSpace(TextUtil.unaccent(movieInTheater),
                TextUtil.unaccent(removeTheAtTheStart(mMovieInCinema.getOriginal_title())))
                || compareAndSkipWhiteSpace(TextUtil.unaccent(movieInTheater),
                TextUtil.unaccent(removeTheAtTheStart(mMovieInCinema.getTitle()))));
    }

    private boolean compareAndSkipWhiteSpace(String s1, String s2) {
        s1 = s1.replaceAll("\\s+", "");
        s2 = s2.replaceAll("\\s+", "");
        return s1.toLowerCase().contains(s2.toLowerCase());
    }

    private String removeTheAtTheStart(String title) {
        if (title.startsWith("The ")) {
            String removeWord = title.substring(0, title.indexOf("The "));
            title = title.substring(title.indexOf("The ") + 4);
        }
        return title;
    }

    private boolean checkMovieInCinema(String cinemaName) {
        boolean valid = false;
        Cursor mCursor = getActivity().getContentResolver().query(NewsFeedContract.MovieDB.CONTENT_URI,
                null, null, null, null);
        mCursor.moveToFirst();
        if (mCursor.getString(0).contains(cinemaName)) {
            valid = true;
        }
        mCursor.close();
        return valid;
    }


    private MarkerOptions defineMarkerOptions(LatLng latlngPos, String title, String description,
                                              List<String> movieTime) {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latlngPos);
        markerOptions.title(title);
        String newLineAddress[] = description.split(",");
        StringBuilder addressBuilder = new StringBuilder();
        for (String s : newLineAddress) {
            addressBuilder.append(s + "\n");
        }

        String timeOfMovie = movieTime != null && !movieTime.isEmpty() ? " #TIME:" + movieTime.get(0) : "";
        markerOptions.snippet(addressBuilder.toString() + timeOfMovie);
        return markerOptions;
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLocation == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                    mLocationRequest, this);
        } else {
            handleNewLocation(mLocation);
        }
    }

    private void handleNewLocation(Location location) {
        if (location != null) {
            initShowTimeIntentService(location);
            double currentLatitude = location.getLatitude();
            double currentLongitude = location.getLongitude();
            LatLng latLng = new LatLng(currentLatitude, currentLongitude);
            //
            // 	mMap.setMyLocationEnabled(true);
            options = new MarkerOptions()
                    .position(latLng)
                    .title("My Position")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

            if (!mMarkerForLocations.contains(options)) {
                mMarkerForLocations.add(options);
            }
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            setUpMap();
        }
    }

    private void initShowTimeIntentService(Location location) {
        if (getActivity() != null) {
            if (LocationUtil.countCinemaDbStored(getActivity()) == 0) {
                Intent intent = new Intent(getActivity(), ShowTimeMovieService.class);
                intent.putExtra(FusedLocationProviderApi.KEY_LOCATION_CHANGED, location);
                getActivity().startService(intent);
                mLocation = location;
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(getActivity(), 9000);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            //Log.i("Out", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
        if (Geocoder.isPresent() && LocationUtil.countCinemaDbStored(getActivity()) > 0
                && !mStateFetchAddress) {
            startIntentService();
            mStateFetchAddress = true;
        }
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(),
                NewsFeedContract.TheaterDB.CONTENT_URI,
                Constants.TheaterQuery.PROJECTION,
                null,
                null,
                null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (getActivity() == null) return;
        ArrayList<Theater> theaters = new ArrayList<Theater>();
        while (data.moveToNext()) {
            Theater theater = new Theater();
            theater.setName(data.getString(Constants.TheaterQuery.NAME));
            theater.setInfo(data.getString(Constants.TheaterQuery.INFO));
            String moviesSplit[] = data.getString(Constants.TheaterQuery.MOVIES).split(";");
            theaters.add(theater);
        }
    }

    private void reinitializeMarkersAndMap(List<Theater> theaters) {
        mMarkerForLocations.clear();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    /**
     * Receiver for data sent from FetchAddressIntentService.
     */
    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        /**
         * Receives data sent from FetchAddressIntentService and updates the UI in MainActivity.
         */
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            if (getActivity() == null) {
                return;
            }
            if (resultCode == Constants.SUCCESS_RESULT) {
                Gson gson = new Gson();
                if (resultData.getString(Constants.RESULT_DATA_KEY) != null) {
                    Type listOfTheaters = new TypeToken<List<Theater>>() {
                    }.getType();
                    mCinemas = gson.fromJson(resultData.getString(Constants.RESULT_DATA_KEY), listOfTheaters);
                    reinitializeMarkersAndMap(mCinemas);
                } else if (resultData.getString(Constants.RESULT_THEATER_KEY) != null) {
                    Type theater = new TypeToken<Theater>() {
                    }.getType();
                    mCurrentTheater = gson.fromJson(resultData.getString(Constants.RESULT_THEATER_KEY), theater);
                }
                createCinemaMarkers();
                handleNewLocation(mLocation);
            } else {
                Toast.makeText(getActivity(), "Can't calculate cinema locations! Turn on the GPS for finding cinemas!", Toast.LENGTH_SHORT).show();
                getFragmentManager().popBackStackImmediate();
            }
        }
    }
}
