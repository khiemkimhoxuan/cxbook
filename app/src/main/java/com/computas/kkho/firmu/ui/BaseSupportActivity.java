package com.computas.kkho.firmu.ui;

import android.content.Intent;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.computas.kkho.firmu.R;
import com.computas.kkho.firmu.adapter.DrawerListAdapter;
import com.computas.kkho.firmu.storage.NewsFeedContract;
import com.computas.kkho.firmu.util.ColorUtil;
import com.computas.kkho.firmu.util.Constants;

/**
 * Created by kkho on 17.01.2015.
 */
public abstract class BaseSupportActivity extends AppCompatActivity {

    private Fragment mFragment;
    public static final String ARG_URI = "_uri";
    private String mTitles[] = {"Home", "List Information of Cinemas", "Cinemas nearby"};
    private DrawerLayout mDrawerLayout;
    private RecyclerView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    // used to store app title
    private CharSequence mTitle;
    private Toolbar mToolbar;
    private int mPreviouslyPosition = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (this instanceof ItemMovieActivity) {
            setContentView(R.layout.activity_base_overlay_toolbar);
        } else if (this instanceof GeoMapActivity || this instanceof
                CinemaMovieInfoActivity) {
            setContentView(R.layout.general_overlay_toolbar);
        } else {
            setContentView(R.layout.activity_base);
        }
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
            getSupportActionBar().setTitle(getTitle());
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }
        if (this instanceof HomeActivity) {

            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mDrawerList = (RecyclerView) findViewById(R.id.left_drawer);
            mDrawerList.setHasFixedSize(true);
            mDrawerList.setLayoutManager(new LinearLayoutManager(this));

            // Set the adapter for the list view
            mDrawerList.setAdapter(new DrawerListAdapter(this, new DrawerItemClickListener()));

            mDrawerToggle = new ActionBarDrawerToggle(
                    this,                  /* host Activity */
                    mDrawerLayout,         /* DrawerLayout object */
                    mToolbar,  /* nav drawer icon to replace 'Up' caret */
                    R.string.drawer_open,  /* "open drawer" description */
                    R.string.drawer_close  /* "close drawer" description */
            ) {

                /**
                 * Called when a drawer has settled in a completely closed state.
                 */
                public void onDrawerClosed(View view) {
                    super.onDrawerClosed(view);
                }

                /**
                 * Called when a drawer has settled in a completely open state.
                 */
                public void onDrawerOpened(View drawerView) {
                    mDrawerList.bringToFront();
                    mDrawerLayout.requestLayout();
                    super.onDrawerOpened(drawerView);
                }
            };
            // Set the drawer toggle as the DrawerListener
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            mDrawerLayout.setDrawerListener(mDrawerToggle);
        }
        if (savedInstanceState == null) {
            mFragment = onCreatePane();
            mFragment.setArguments(intentToFragmentArguments(getIntent()));
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.root_container, mFragment, "single_pane")
                    .commit();
        }
    }

    protected abstract Fragment onCreatePane();

    public Fragment getFragment() {
        return mFragment;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (this instanceof HomeActivity) {
            // Sync the toggle state after onRestoreInstanceState has occurred.
            mDrawerToggle.syncState();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (this instanceof HomeActivity) {
                    return false;
                } else if (this instanceof ItemMovieActivity ||
                        this instanceof SearchResultActivity) {
                    supportFinishAfterTransition();
                } else {
                    NavUtils.navigateUpFromSameTask(this);
                }
                return true;
        }
        if (mDrawerToggle != null && mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static Bundle intentToFragmentArguments(Intent intent) {
        Bundle arguments = new Bundle();
        if (intent == null) {
            return arguments;
        }

        final Uri data = intent.getData();
        if (data != null) {
            arguments.putParcelable(ARG_URI, data);
        }

        final Bundle extras = intent.getExtras();
        if (extras != null) {
            arguments.putAll(intent.getExtras());
        }
        return arguments;
    }

    public static Intent fragmentArgumentToIntent(Bundle arguments) {
        Intent intent = new Intent();
        if (arguments == null) {
            return intent;
        }

        final Uri data = arguments.getParcelable(ARG_URI);
        if (data != null) {
            intent.setData(data);
        }

        intent.putExtras(arguments);
        intent.removeExtra(ARG_URI);
        return intent;
    }

    class DrawerItemClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int position = mDrawerList.getChildAdapterPosition(v);
            if (mPreviouslyPosition != position) {
                mPreviouslyPosition = position;
                selectItem(position);
            }
        }
    }

    /**
     * Swaps fragments in the main content view
     */
    private void selectItem(int position) {
        if (this instanceof ItemMovieActivity) {
            return;
        }
        Fragment mFragment = null;
        switch (position) {
            case 0:
                mFragment = new HomeFragment();
                break;
            case 1:
                mFragment = new TheaterInfoFragment();
                break;
            case 2:
                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.SHOW_ALL_CINEMAS, true);
                Cursor cursor = getContentResolver().query(NewsFeedContract.TheaterDB.CONTENT_URI,
                        new String[]{"count(*) AS count"}, null, null, null);
                cursor.moveToFirst();
                if (cursor.getCount() > 0) {
                    mFragment = new GeoMapFragment();
                    mFragment.setArguments(bundle);
                } else {
                    Toast.makeText(this, "Cinema Location is loading", Toast.LENGTH_LONG).show();
                }
                break;

            default:
                break;
        }

        if (mFragment != null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.root_container, mFragment).
                    commit();

            ColorUtil.clearColorTextView(mDrawerList, this);
            setPositionDrawerList(position);
            mDrawerLayout.closeDrawer(mDrawerList);
        }

    }

    public void setPositionDrawerList(int position) {
        if (mDrawerList != null) {
            ((DrawerListAdapter) mDrawerList.getAdapter()).selectPosition(position);
            setTitle(mTitles[position]);
        }
    }

    protected int getActionBarSize() {
        TypedValue typedValue = new TypedValue();
        int[] textSizeAttr = new int[]{R.attr.actionBarSize};
        int indexOfAttrTextSize = 0;
        TypedArray a = obtainStyledAttributes(typedValue.data, textSizeAttr);
        int actionBarSize = a.getDimensionPixelSize(indexOfAttrTextSize, -1);
        a.recycle();
        return actionBarSize;
    }

    protected int getScreenHeight() {
        return findViewById(android.R.id.content).getHeight();
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    public RecyclerView getDrawerList() {
        return mDrawerList;
    }

}
