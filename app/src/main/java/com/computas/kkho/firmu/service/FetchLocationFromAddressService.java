package com.computas.kkho.firmu.service;

import android.app.IntentService;
import android.content.Intent;
import android.database.Cursor;
import android.location.Address;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.computas.kkho.firmu.model.Theater;
import com.computas.kkho.firmu.storage.NewsFeedContract;
import com.computas.kkho.firmu.util.CinemaUtil;
import com.computas.kkho.firmu.util.Constants;
import com.computas.kkho.firmu.util.LocationUtil;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class FetchLocationFromAddressService extends IntentService {
    private ResultReceiver mReceiver;

    public FetchLocationFromAddressService() {
        super("FetchLocationFromAddressService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        mReceiver = intent.getParcelableExtra(Constants.RECEIVER);
        Theater currentTheater = (Theater) intent.getSerializableExtra(Constants.CINEMA_LOCATION_RECEIVER);
        if (currentTheater != null) {
            getInformationOfCurrentLocation(currentTheater);
        } else {
            createTheaterList();
        }
    }

    private void getInformationOfCurrentLocation(Theater theater) {

        try {
            String addressInfo = CinemaUtil.cinemaOutdatedIn(theater.getInfo());
            List<Address> addressList = LocationUtil.geocoderFactory(this).getFromLocationName(addressInfo, 1);
            if (addressList != null && !addressList.isEmpty()) {
                Address address = addressList.get(0);
                theater.setLatlng(new LatLng(address.getLatitude(), address.getLongitude()));
                theater.setInfo(addressInfo);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        deliverResultToReceiverLocationCinema(Constants.SUCCESS_RESULT, theater);
    }


    private void createTheaterList() {
        ArrayList<Theater> theaterList = new ArrayList<>();
        Cursor cursor = getContentResolver().query(NewsFeedContract.TheaterDB.CONTENT_URI,
                null, null, null, null);
        cursor.moveToFirst();

        if (cursor.getCount() > 0) {

            do {
                Theater theater = new Theater();
                theater.setName(cursor.getString(1));
                theater.setInfo(cursor.getString(2));
                theater.setMoviesToShow(cursor.getString(3));
                theater.setMovieShowTimeInfo(cursor.getString(4));

                try {
                    String addressInfo = CinemaUtil.cinemaOutdatedIn(theater.getInfo());
                    List<Address> addressList = LocationUtil.geocoderFactory(this).getFromLocationName(theater.getInfo(), 1);
                    if (addressList != null && !addressList.isEmpty()) {
                        Address address = addressList.get(0);
                        theater.setLatlng(new LatLng(address.getLatitude(), address.getLongitude()));
                        theater.setInfo(addressInfo);
                        theaterList.add(theater);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        if (!theaterList.isEmpty()) {
            deliverResultToReceiverList(Constants.SUCCESS_RESULT, theaterList);
        } else {
            deliverResultToReceiverList(Constants.FAILURE_RESULT, theaterList);
        }
    }

    private void deliverResultToReceiverList(int resultCode, List<Theater> message) {
        Bundle bundle = new Bundle();
        Gson gson = new Gson();
        Type listOfTheaters = new TypeToken<List<Theater>>() {
        }.getType();
        bundle.putString(Constants.RESULT_DATA_KEY, gson.toJson(message, listOfTheaters));
        mReceiver.send(resultCode, bundle);
    }

    private void deliverResultToReceiverLocationCinema(int resultCode, Theater locationCinema) {
        Bundle bundle = new Bundle();
        Gson gson = new Gson();
        Type cinema = new TypeToken<Theater>() {
        }.getType();
        bundle.putString(Constants.RESULT_THEATER_KEY, gson.toJson(locationCinema, cinema));
        mReceiver.send(resultCode, bundle);
    }
}
