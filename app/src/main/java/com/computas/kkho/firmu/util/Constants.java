package com.computas.kkho.firmu.util;

import com.computas.kkho.firmu.storage.NewsFeedContract;

/**
 * Created by kkh on 21.03.2015.
 */
public class Constants {

    public static final String NO_CINEMAS = "com.computas.kkho.firmu.NO_CINEMA";
    public static final int FRAGMENT_CODE = 25;
    public static final String BACKGROUND_COLOR_SWATCH = "com.computas.kkho.firmu.BACKGROUND";
    public static final String SHOW_ALL_CINEMAS = "com.computas.kkho.firmu.SHOW_ALL_CINEMAS";
    public static final String CINEMA_LOCATION = "com.computas.kkho.firmu.CINEMA_LOCATION";
    public static final String RESULT_THEATER_KEY = "com.computas.kkho.firmu.RESULT_THEATER_KEY";
    public static final String CINEMA_LOCATION_RECEIVER = "com.computas.kkho.firmu.CINEMA_LOCATION_RECEIVER";
    public static final int MAX_COLOR_PALETTE = 24;
    public static final String CONFIGURATION_SETUP = "com.computas.kkho.firmu.CONFIGURATION_SETUP";
    public static final String SHARED_PREFERENCE = "com.computas.kkho.firmu.SHARED_PREFERENCE";
    public static final String FIRST_MOVIE_LOOKUP = "com.computas.kkho.firmu.FIRST_MOVIE_LOOKUP";

    public interface DrawerDefaultItems {
        public static final String HOME = "Home";
        public static final String PROFILE = "Profile";
    }

    public interface DrawerMoviesItems {
        public static final String SHOW_MOVIE_PLAYING = "Show Cinemas That Plays The Movie";
        public static final String FIND_NEAREST_CINEMAS = "Find Nearest Cinemas";
    }

    public static final int SUCCESS_RESULT = 0;
    public static final int FAILURE_RESULT = 1;
    public static final String PACKAGE_NAME =
            "com.computas.kkho.firmu";
    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
    public static final String RESULT_DATA_KEY = PACKAGE_NAME +
            ".RESULT_DATA_KEY";
    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME +
            ".LOCATION_DATA_EXTRA";
    public static final String TEXT_COLOR_SWATCH = "com.firmu.kkho.TEXT_COLOR";
    public static final String MOVIE_PLAYING_CINEMA = "com.firmu.kkho.MOVIE_PLAYING_CINEMA";
    public static final String PREFERRED_QUALITY = "w342";

    public static interface TheaterQuery {
        public static String[] PROJECTION = {
                NewsFeedContract.TheaterDB.NAME,
                NewsFeedContract.TheaterDB.INFO,
                NewsFeedContract.TheaterDB.MOVIES,
                NewsFeedContract.TheaterDB.MOVIESTIME
        };

        public static int NAME = 0;
        public static int INFO = 1;
        public static int MOVIES = 2;
        public static int MOVIESTIME = 3;
    }


}
