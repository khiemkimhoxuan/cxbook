package com.computas.kkho.firmu.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.computas.kkho.firmu.R;

/**
 * Created by kkh on 11.05.2015.
 */
public class DrawerListAdapter extends RecyclerView.Adapter<DrawerItemHolder> {
    private String mTitles[] = {"Home", "List Information of Cinemas", "Cinemas nearby"};
    private int mIcons[] = {R.drawable.ic_home_black_36dp, R.drawable.ic_local_movies_black_36dp,
            R.drawable.ic_pin_drop_black_36dp};

    private Context mContext;
    private int mSelectedPosition;
    private View.OnClickListener mClickListener;


    public DrawerListAdapter(Context context, View.OnClickListener clickListener) {
        mContext = context;
        mClickListener = clickListener;
    }

    @Override
    public DrawerItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.drawer_list_item, parent, false);
        return new DrawerItemHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DrawerItemHolder holder, int position) {
        String title = mTitles[position];
        int icons = mIcons[position];
        holder.vDrawerText.setText(title);
        holder.vDrawerIcon.setImageResource(icons);
        holder.itemView.setOnClickListener(mClickListener);
        if (mSelectedPosition == position) {
            holder.itemView.setBackgroundColor(holder.itemView.getContext().getResources().getColor(R.color.theme_color_darker));
        } else {
            holder.itemView.setBackgroundColor(Color.TRANSPARENT);
        }

    }

    public void selectPosition(int position) {
        int lastPosition = mSelectedPosition;
        mSelectedPosition = position;
        notifyItemChanged(lastPosition);
        notifyItemChanged(position);
    }

    @Override
    public int getItemCount() {
        return mTitles.length;
    }
}
