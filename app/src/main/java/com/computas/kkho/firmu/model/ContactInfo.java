package com.computas.kkho.firmu.model;

/**
 * Created by kkh on 26.02.2015.
 */
public class ContactInfo {
    public String name;
    public String lastName;
    public String email;
    public String address;
    public String phoneNumber;
    public static final String NAME_PREFIX = "Name_";
    public static final String LASTNAME_PREFIX = "Lastname_";
    public static final String EMAIL_PREFIX = "Email_";
    public static final String ADDRESS_PREFIX = "Address_";
    public static final String PHONE_NUMBER_PREFIX = "PHONE_";
}
