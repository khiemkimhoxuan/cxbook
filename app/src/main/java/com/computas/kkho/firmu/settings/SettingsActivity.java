package com.computas.kkho.firmu.settings;

import android.app.Fragment;

import com.computas.kkho.firmu.ui.BaseActivity;

/**
 * Created by kkho on 17.01.2015.
 */
public class SettingsActivity extends BaseActivity {

    @Override
    protected Fragment onCreatePane() {
        return new SettingsFragment();
    }
}
