package com.computas.kkho.firmu.util;

import com.computas.kkho.firmu.model.BoxOfficeMovie;
import com.computas.kkho.firmu.model.Theater;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by kkh on 22.03.2015.
 */
public class CinemaUtil {

    public interface CinemasOutdated {
        public static String LOERENSKOG = "Festplassen 1, 1473 Lørenskog, Norway";
        public static String KLINGENBERG = "Olav Vs gate 4, 0161 Oslo, Norway";
        public static String VICTORIA_KINO = "Ruseløkkveien 14, 0251 Oslo, Norway";
    }

    public static String cinemaOutdatedIn(String outdatedCinema) {
        String cinemaIn = outdatedCinema;
        if (outdatedCinema.contains("Lørenskog Kulturesenter")) {
            cinemaIn = CinemasOutdated.LOERENSKOG;
        } else if (outdatedCinema.contains("Olav V gate 4, Oslo")) {
            cinemaIn = CinemasOutdated.KLINGENBERG;
        } else if (outdatedCinema.contains("Ruseløkkveien 14 v.")) {
            cinemaIn = CinemasOutdated.VICTORIA_KINO;
        } else if (outdatedCinema.contains("20000 Century Boulevard, Germantown, MD")) {
            cinemaIn = "20000 Century Blvd, Germantown, MD 20874";
        }
        cinemaIn = cinemaIn.replaceAll("[Bb]oulevard", "blvd");
        return cinemaIn;
    }

    public boolean latestResultSearchedMovie(List<BoxOfficeMovie> movieList, BoxOfficeMovie currentMovie) {
        boolean returnResult = false;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date;
        Date maxDate = new Date(0);
        Date currentMovieDate = new Date(0);
        try {
            if (currentMovie.getRelease_date().equals("") && movieList.size() > 1) {
                returnResult = false;
            } else {
                currentMovieDate = dateFormat.parse(currentMovie.getRelease_date());
                for (BoxOfficeMovie boxOfficeMovie : movieList) {
                    if (boxOfficeMovie.getRelease_date().equals("")) {
                        continue;
                    }
                    date = dateFormat.parse(boxOfficeMovie.getRelease_date());
                    if (currentMovieDate.compareTo(date) > 0) {
                        maxDate = date;
                    }
                }
            }
            returnResult = currentMovieDate.compareTo(maxDate) >= 0;
        } catch (ParseException pe) {
            pe.getStackTrace();

        }
        return returnResult;
    }

    public static boolean hasAlreadyCinema(BoxOfficeMovie cinemaMovie, List<BoxOfficeMovie> mListItems) {
        if (!mListItems.isEmpty()) {
            for (BoxOfficeMovie boxOfficeMovie : mListItems) {
                if (boxOfficeMovie.getId().equals(cinemaMovie.getId())
                        && boxOfficeMovie.getOriginal_title().equals(cinemaMovie.getOriginal_title())
                        && boxOfficeMovie.getTitle().equals(cinemaMovie.getOriginal_title())
                        && boxOfficeMovie.getRelease_date().equals(cinemaMovie.getRelease_date())) {
                    return true;
                }
            }
        }
        return false;
    }

    public static ArrayList<String> allMoviesInCinema(ArrayList<Theater> mTheaters) {
        ArrayList<String> movieCollector = new ArrayList<>();
        if (mTheaters != null) {
            for (Theater theater : mTheaters) {
                String moviesPlaying[] = theater.getMoviesToShow().split(";");
                for (String movies : moviesPlaying) {
                    if (movieCollector.isEmpty()) {
                        movieCollector.add(movies);
                    } else {
                        if (!movieCollector.toString().
                                contains(movies)) {
                            movieCollector.add(movies);
                        }
                    }
                }
            }
        }
        return movieCollector;
    }


    public static ArrayList<BoxOfficeMovie> alreadyStoredCinemaMovie(List<BoxOfficeMovie> list,
                                                                     ArrayList<String> mMoviesInCinema) {
        ArrayList<BoxOfficeMovie> result = new ArrayList<>();
        for (BoxOfficeMovie movie : list) {
            for (String s : mMoviesInCinema) {
                if (!movie.getOriginal_title().equals(s)) {
                    result.add(movie);
                }
            }
        }
        return result;
    }
}
