package com.computas.kkho.firmu.rest;

import com.computas.kkho.firmu.model.BoxOfficeMovieWrapper;
import com.computas.kkho.firmu.model.BoxOfficeMovieWrapperNowPlaying;
import com.computas.kkho.firmu.model.BoxOfficeSearchMovieWrapper;
import com.computas.kkho.firmu.model.ConfigurationResponse;
import com.computas.kkho.firmu.model.Credits;
import com.computas.kkho.firmu.model.DiscoverWrapper;
import com.computas.kkho.firmu.model.MovieDetail;
import com.computas.kkho.firmu.model.MovieImageWrapper;
import com.computas.kkho.firmu.model.MovieTrailer;
import com.computas.kkho.firmu.model.ReviewWrapper;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by kkh on 26.03.2015.
 */
public interface MovieDbApi {

    @GET("/movie/popular")
    void getPopularMovies(@Query("api_key") String apiKey,
                          Callback<BoxOfficeMovieWrapper> callback);

    @GET("/movie/now_playing")
    void getNowPlayingMovies(@Query("api_key") String apiKey,
                             Callback<BoxOfficeMovieWrapperNowPlaying> callback);

    @GET("/movie/{id}")
    void getMovieDetail(@Query("api_key") String apiKey,
                        @Path("id") String id,
                        Callback<MovieDetail> callback);

    @GET("/movie/{id}/videos")
    void getMovieTrailer(@Query("api_key") String apiKey,
                         @Path("id") String id,
                         Callback<MovieTrailer> callback);

    @GET("/movie/{id}/credits")
    void getCreditsList(@Query("api_key") String apiKey,
                        @Path("id") String id,
                        Callback<Credits> callback);

    @GET("/movie/popular")
    void getPopularMoviesByPage(@Query("api_key") String apiKey,
                                @Query("page") String page,
                                Callback<BoxOfficeMovieWrapper> callback);

    @GET("/movie/now_playing")
    void getNowPlayingMoviesByPage(@Query("api_key") String apiKey,
                                   @Query("page") String page,
                                   Callback<BoxOfficeMovieWrapperNowPlaying> callback);

    @GET("/search/movie")
    void getSearchedMoviesByPage(@Query("api_key") String apiKey,
                                 @Query("query") String query,
                                 @Query("page") String page,
                                 Callback<BoxOfficeSearchMovieWrapper> callback);

    @GET("/discover/movie")
    void getDiscoveryMoviesByPage(@Query("api_key") String apiKey,
                                  @Query("year") int year,
                                  @Query("page") String page,
                                  Callback<DiscoverWrapper> callback);

    @GET("/search/movie")
    void getSearchedMovie(@Query("api_key") String apiKey,
                          @Query("query") String query,
                          Callback<BoxOfficeSearchMovieWrapper> callback);

    @GET("/movie/now_playing")
    void getTheaterMoviesByPage(@Query("api_key") String apiKey,
                                @Query("page") String page,
                                Callback<BoxOfficeMovieWrapper> callback);

    @GET("/configuration")
    void getConfiguration(@Query("api_key") String apiKey,
                          Callback<ConfigurationResponse> response);

    @GET("/discover/movie")
        //TTODO add primary release date.gte and lte
    void getDiscoveryMovie(@Query("api_key") String apiKey,
                           @Query("year") int year,
                           Callback<DiscoverWrapper> callback);

    @GET("/movie/{id}/reviews")
    void getReviews(@Query("api_key") String apiKey,
                    @Path("id") String id,
                    Callback<ReviewWrapper> response);

    @GET("/movie/{id}/images")
    void getImages(@Query("api_key") String apiKey,
                   @Path("id") String movieId,
                   Callback<MovieImageWrapper> response);

}
