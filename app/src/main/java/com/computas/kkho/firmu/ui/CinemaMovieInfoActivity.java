package com.computas.kkho.firmu.ui;

import android.support.v4.app.Fragment;

/**
 * Created by kkh on 02.05.2015.
 */
public class CinemaMovieInfoActivity extends BaseSupportActivity {

    @Override
    public Fragment onCreatePane() {
        return new CinemaMovieInfoFragment();
    }
}
