package com.computas.kkho.firmu.settings;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.computas.kkho.firmu.R;

/**
 * Created by kkho on 17.01.2015.
 */
public class SettingsFragment extends PreferenceFragment implements
        SharedPreferences.OnSharedPreferenceChangeListener {

    public static final String KEY_PREF_POPUP = "pref_key_popup";
    public static final String KEY_PREF_USE_OWN_NUMBER = "pref_key_use_own_number";
    public static final String KEY_PREF_OWN_NUMBER = "pref_key_own_number";
    public static final String KEY_PREF_INITIATE_CALL = "pref_key_initiate_call";
    public static final String KEY_PREF_STORED_LOCATION = "pref_key_stored_location";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        setRetainInstance(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

    }
}
