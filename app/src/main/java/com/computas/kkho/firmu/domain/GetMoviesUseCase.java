package com.computas.kkho.firmu.domain;

import com.computas.kkho.firmu.model.BoxOfficeMovieWrapper;
import com.computas.kkho.firmu.model.BoxOfficeMovieWrapperNowPlaying;
import com.computas.kkho.firmu.model.DiscoverWrapper;

/**
 * Created by kkh on 26.03.2015.
 */
public interface GetMoviesUseCase extends UseCase {
    public void onPopularMoviesReceived(BoxOfficeMovieWrapper response);

    public void requestPopularMovies();

    public boolean requestSearchMovies(String query);

    public void onNowPlayingMoviesReceived(BoxOfficeMovieWrapperNowPlaying response);

    public void onDiscoverMoviesReceived(DiscoverWrapper response);

    public void requestNowPlayingMovies();

    public void requestDiscoverMovies();

    public void sendNowPlayingMoviesToPresenter(BoxOfficeMovieWrapperNowPlaying response);

    public void sendDiscoverMoviesToPresenter(DiscoverWrapper response);

    public void sendMoviesToPresenter(BoxOfficeMovieWrapper response);

    public void unRegister();
}
