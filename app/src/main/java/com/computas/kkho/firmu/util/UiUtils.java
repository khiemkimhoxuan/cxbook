package com.computas.kkho.firmu.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.computas.kkho.firmu.R;
import com.computas.kkho.firmu.mvp.presenters.MoviesPresenter;
import com.computas.kkho.firmu.mvp.presenters.MoviesSearchPresenter;
import com.computas.kkho.firmu.ui.HomeActivity;
import com.computas.kkho.firmu.ui.SearchResultActivity;

import java.io.File;
import java.util.Collection;
import java.util.HashSet;

/**
 * Created by kkh on 03.05.2015.
 */
public class UiUtils {
    public static float getProgress(int value, int min, int max) {
        if (min == max) {
            throw new IllegalArgumentException("Max (" + max + ") cannot equal min (" + min + ")");
        }

        return (value - min) / (float) (max - min);
    }

    public static void createSearchWidget(final AppCompatActivity activity, final Menu menu, final MoviesSearchPresenter moviesSearchPresenter,
                                          final MoviesPresenter moviesPresenter) {
        if (activity == null) return;
        final MenuItem searchItem = menu.findItem(R.id.action_search);

        SearchManager searchManager = (SearchManager) activity.getSystemService(Context.SEARCH_SERVICE);

        final SearchView searchView = (SearchView) searchItem.getActionView();
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(activity.getComponentName()));
            searchView.setQueryHint("Search for movies");
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

                @Override
                public boolean onQueryTextSubmit(String query) {
                    Intent intent = new Intent(activity, SearchResultActivity.class);
                    intent.putExtra(SearchManager.QUERY, query);
                    if (activity instanceof HomeActivity) {
                        activity.startActivity(intent);
                    } else {
                        moviesSearchPresenter.refreshMovieList();
                        moviesSearchPresenter.setQuery(query);
                        moviesSearchPresenter.beginSearch(query);
                    }


                    searchView.setIconified(true);
                    searchView.clearFocus();
                    searchItem.collapseActionView();
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    // TODO Auto-generated method stub
                    return false;
                }
            });
        }
    }

    public static void removeDuplicates(Collection list) {
        HashSet set = new HashSet(list);
        list.clear();
        list.addAll(set);
    }

    /**
     * Deletes a directory tree recursively.
     */
    public static void deleteDirectoryTree(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles()) {
                deleteDirectoryTree(child);
            }
        }

        fileOrDirectory.delete();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void startTransitionSharedElement(View touchedView, Activity activity, int moviePosition,
                                                    Intent movieDetailActivityIntent) {
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation(
                        activity, touchedView, "moviecover");
        activity.startActivity(movieDetailActivityIntent, options.toBundle());
    }

    public static Snackbar initSnackBar(View view, Context context) {
        return Snackbar.make(view, "Loading more films", Snackbar.LENGTH_LONG)
                .setActionTextColor(context.getResources().getColor(R.color.smoke_white));
    }
}
