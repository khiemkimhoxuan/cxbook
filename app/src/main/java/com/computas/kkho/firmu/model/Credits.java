package com.computas.kkho.firmu.model;

import java.util.List;

/**
 * Created by kkh on 11.04.2015.
 */
public class Credits {
    private int id;
    private List<Cast> cast;

    public List<Cast> getCast() {
        return cast;
    }

    public int getId() {
        return id;
    }

    @SuppressWarnings("UnusedDeclaration")
    public class Cast {
        private int cast_id;
        private String character;
        private String credit_id;
        private int id;
        private String name;
        private int order;
        private String profile_path;

        public int getCast_id() {
            return cast_id;
        }

        public String getCharacter() {
            return character;
        }

        public String getCredit_id() {
            return credit_id;
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public int getOrder() {
            return order;
        }

        public String getProfile_path() {
            return profile_path;
        }
    }
}


