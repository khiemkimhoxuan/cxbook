package com.computas.kkho.firmu.ui;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.computas.kkho.firmu.Firmu;
import com.computas.kkho.firmu.R;
import com.computas.kkho.firmu.model.BoxOfficeMovie;
import com.computas.kkho.firmu.model.Credits;
import com.computas.kkho.firmu.model.MovieTrailer;
import com.computas.kkho.firmu.model.Review;
import com.computas.kkho.firmu.mvp.presenters.MovieDetailPresenter;
import com.computas.kkho.firmu.mvp.views.DetailView;
import com.computas.kkho.firmu.util.ColorUtil;
import com.computas.kkho.firmu.util.Constants;
import com.computas.kkho.firmu.util.DateUtil;
import com.computas.kkho.firmu.util.ScrollUtils;
import com.computas.kkho.firmu.util.ToolbarHelper;
import com.computas.kkho.firmu.util.UiUtils;
import com.computas.kkho.firmu.widget.ObservableScrollView;
import com.computas.kkho.firmu.widget.ObservableScrollViewCallbacks;
import com.computas.kkho.firmu.widget.ScrollState;
import com.google.gson.Gson;
import com.melnykov.fab.FloatingActionButton;
import com.nineoldandroids.view.ViewHelper;
import com.squareup.leakcanary.RefWatcher;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.InjectViews;
import butterknife.OnClick;

public class ItemMovieFragment extends Fragment implements
        DetailView, Palette.PaletteAsyncListener, ObservableScrollViewCallbacks {

    private int mReviewsColor = -1;
    private int mReviewsAuthorColor = -1;
    private int mTitleColor = -1;
    private int mBackgroundColor = -1;
    private String mTitles[] = {
            Constants.DrawerMoviesItems.SHOW_MOVIE_PLAYING,
            Constants.DrawerMoviesItems.FIND_NEAREST_CINEMAS};

    private ActionBarDrawerToggle mDrawerToggle;
    private BoxOfficeMovie mBoxOfficeMovie;
    private MovieDetailPresenter mMovieDetailPresenter;
    private int mParallaxImageHeight;

    Toolbar mToolbar;

    @InjectView(R.id.cardview_detailed_info)
    ObservableScrollView mRootInfoMovieScrollView;
    @InjectView(R.id.detailed_image)
    ImageView mMovieImageView;
    @InjectView(R.id.detailed_movie_title)
    TextView mTitleTextView;
    @InjectView(R.id.premier_date_label)
    TextView mPremierDateLabelTextView;
    @InjectView(R.id.premier_date_text)
    TextView mPremiereDateTimeTextView;
    @InjectView(R.id.movie_icon_fab)
    FloatingActionButton mFabButton;
    @InjectView(R.id.frame_bar_info)
    FrameLayout mDetailedInfoLayout;
    @InjectView(R.id.description_text)
    TextView mSynopsisText;
    @InjectView(R.id.vote_average_display)
    TextView mAverageScore;
    @InjectView(R.id.castList)
    TextView mCastList;
    @InjectView(R.id.score_and_cast)
    TextView mScoreCastTitle;

    @InjectViews({R.id.progressbarMovieTitle, R.id.progressbarCast, R.id.progressbarDescription})
    List<ProgressBar> mProgressBars;

    //Colored titles
    @InjectView(R.id.description_text_title)
    TextView mSynopsisTextTitle;

    private Palette.Swatch mBrightSwatch;

    private String YouTubeURI = "http://www.youtube.com/watch?v=";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Gson gson = new Gson();
        mToolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        mBoxOfficeMovie = gson.fromJson(getArguments()
                        .getString(HomeFragment.BOX_OFFICE_MOVIE),
                BoxOfficeMovie.class);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            // Set paddingTop of toolbar to height of status bar.
            // Fixes statusbar covers toolbar issue
            if (mToolbar != null) {
                mToolbar.setPadding(0, ColorUtil.getStatusBarHeight(getActivity()), 0, 0);
            }
        }
        setRetainInstance(true);
        setHasOptionsMenu(true);

    }


    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        mParallaxImageHeight = mMovieImageView.getHeight() - mDetailedInfoLayout.getHeight();
        float alpha = 1 - (float) Math.max(0, mParallaxImageHeight - scrollY) / mParallaxImageHeight;
        mToolbar.setBackgroundColor(ScrollUtils.getColorWithAlpha(alpha, mBackgroundColor));
        ViewHelper.setTranslationY(mMovieImageView, scrollY / 2);
    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.item_movie_detailed, container, false);
        ButterKnife.inject(this, view);
        getActivity().setTitle(getActivity().getTitle());
        mRootInfoMovieScrollView.setScrollViewCallbacks(this);
        mPremiereDateTimeTextView.setText(DateUtil.formatYearMonthDayDate(mBoxOfficeMovie.getRelease_date()));
        mFabButton.attachToScrollView(mRootInfoMovieScrollView);
        mMovieDetailPresenter = new MovieDetailPresenter(this, mBoxOfficeMovie.getId());
        mMovieDetailPresenter.start();
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mMovieDetailPresenter.stop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        RefWatcher refWatcher = Firmu.getRefWatcher(getActivity());
        refWatcher.watch(this);
        UiUtils.deleteDirectoryTree(getActivity().getCacheDir());
    }


    @Override
    public void showFilmCover(Bitmap bitmap) {
        if (bitmap == null || bitmap.isRecycled()) {
            Intent i = new Intent(getActivity(), HomeActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getActivity().startActivity(i);
            return;
        }
        mMovieImageView.setImageBitmap(bitmap);
        new Palette.Builder(bitmap)
                .maximumColorCount(Constants.MAX_COLOR_PALETTE)
                .generate(this);
    }

    @Override
    public void setName(String title) {
        if (getActivity() == null || mTitleTextView == null) {
            return;
        }
        mTitleTextView.setText(title);
        mProgressBars.get(0).setVisibility(View.GONE);
    }

    @Override
    public void setDescription(String description) {
        if (getActivity() == null || mSynopsisText == null) {
            return;
        }
        mSynopsisText.setText(description);
        mProgressBars.get(2).setVisibility(View.GONE);
    }

    @Override
    public void setHomepage(String homepage) {

    }

    @Override
    public void setCompanies(String companies) {

    }

    @Override
    public void setAverageRating(String rating) {
        if (getActivity() == null || mAverageScore == null) {
            return;
        }
        mAverageScore.setText(rating);
    }

    @Override
    public void setTagline(String tagline) {

    }

    @Override
    public void showMovieTrailer(MovieTrailer movieTrailer) {
        YouTubeURI += movieTrailer.getResults().get(0).getKey();
    }

    @Override
    public void finish(String cause) {

    }

    @Override
    public void showConfirmationView() {

    }

    @Override
    public void animateConfirmationView() {

    }

    @Override
    public void startClosingConfirmationView() {

    }

    @Override
    public void showReviews(List<Review> results) {

    }

    @Override
    public void showMovieImage(String url) {
        if (mMovieImageView != null) {
            Picasso.with(getActivity()).load(url)
                    .fit().centerCrop()
                    .memoryPolicy(MemoryPolicy.NO_STORE)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(mMovieImageView);
        }
    }


    @Override
    public void showCredits(List<Credits.Cast> results) {
        if (getActivity() == null || results.isEmpty() || mCastList == null) {
            return;
        }
        if (mCastList != null) {
            StringBuffer stringBuffer = new StringBuffer();
            for (Credits.Cast cast : results) {
                stringBuffer.append(cast.getName() + ",\n");
            }
            stringBuffer.setLength(stringBuffer.length() - 2);
            mCastList.setText(stringBuffer.toString());
            mProgressBars.get(1).setVisibility(View.GONE);
        }
    }

    @Override
    public void onGenerated(Palette palette) {

        if (palette != null) {
            final Palette.Swatch darkVibrantSwatch = palette.getDarkVibrantSwatch();
            final Palette.Swatch darkMutedSwatch = palette.getDarkMutedSwatch();
            final Palette.Swatch lightVibrantSwatch = palette.getLightVibrantSwatch();
            final Palette.Swatch lightMutedSwatch = palette.getLightMutedSwatch();
            final Palette.Swatch vibrantSwatch = palette.getVibrantSwatch();

            final Palette.Swatch backgroundAndContentColors = (darkVibrantSwatch != null)
                    ? darkVibrantSwatch : darkMutedSwatch;

            final Palette.Swatch titleAndFabColors = (darkVibrantSwatch != null)
                    ? lightVibrantSwatch : lightMutedSwatch;
            setBackgroundAndFabContentColors(backgroundAndContentColors);
            setActionbarColor(backgroundAndContentColors);
            setHeadersTiltColors(titleAndFabColors);
        }
    }


    public void setBackgroundAndFabContentColors(Palette.Swatch swatch) {
        if (swatch != null) {
            int rgb = swatch.getRgb();
            if (mPremierDateLabelTextView != null && mPremiereDateTimeTextView != null &&
                    mScoreCastTitle != null && mDetailedInfoLayout != null
                    && mSynopsisTextTitle != null) {
                mPremierDateLabelTextView.setTextColor(rgb);
                mPremiereDateTimeTextView.setTextColor(rgb);
                mScoreCastTitle.setTextColor(rgb);
                mDetailedInfoLayout.setBackgroundColor(rgb);
                mSynopsisTextTitle.setTextColor(rgb);
            }
            if (mProgressBars != null) {
                mProgressBars.get(0).getIndeterminateDrawable().setColorFilter(swatch.getTitleTextColor(), PorterDuff.Mode.SRC_IN);
                mProgressBars.get(1).getIndeterminateDrawable().setColorFilter(rgb, PorterDuff.Mode.SRC_IN);
                mProgressBars.get(2).getIndeterminateDrawable().setColorFilter(rgb, PorterDuff.Mode.SRC_IN);
            }
            mFabButton.setColorNormal(rgb);
            mFabButton.setColorPressed(rgb);
        }
    }

    public void setActionbarColor(Palette.Swatch swatch) {
        int rgb = R.color.theme_color;
        int textColor = mFabButton.getColorNormal();
        if (swatch != null) {
            rgb = swatch.getRgb();
            textColor = swatch.getTitleTextColor();

            ToolbarHelper.colorizeToolbar(mToolbar, textColor, getActivity());
            ColorUtil.setActionbarColor(textColor, mToolbar);
        }
        mTitleColor = textColor;
        mBackgroundColor = rgb;
        mToolbar.setBackgroundColor(Color.TRANSPARENT);
        StateListDrawable mStateList = new StateListDrawable();
        mStateList.addState(new int[]{android.R.attr.state_pressed, android.R.attr.state_enabled}, new ColorDrawable(mBackgroundColor));
        mStateList.addState(new int[]{android.R.attr.state_activated, android.R.attr.state_enabled}, new ColorDrawable(mBackgroundColor));
        mStateList.addState(new int[]{android.R.attr.state_focused, android.R.attr.state_enabled}, new ColorDrawable(mBackgroundColor));
        mStateList.addState(new int[]{}, new ColorDrawable(R.color.background_material_light));
    }

    public void setHeadersTiltColors(Palette.Swatch swatch) {
        if (swatch != null) {
            int rgb = swatch.getRgb();
            mBrightSwatch = swatch;
            mTitleTextView.setTextColor(rgb);
            mReviewsAuthorColor = rgb;
            mRootInfoMovieScrollView.setBackgroundColor(rgb);
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getActivity().getWindow().setStatusBarColor(rgb);
            }
        }
    }

    public void setVibrantElements(Palette.Swatch swatch) {
        mFabButton.setColorNormal(swatch.getRgb());
    }

    @OnClick({R.id.movie_icon_fab, R.id.video_play})
    public void clickFabButton(View view) {
        switch (view.getId()) {
            case R.id.movie_icon_fab:
                handleFabEvent();
                break;
            case R.id.video_play:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(YouTubeURI)));
            default:
                break;
        }
    }

    private void handleFabEvent() {
        android.support.v4.app.Fragment mFragment = null;
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.TEXT_COLOR_SWATCH, mTitleColor);
        bundle.putInt(Constants.BACKGROUND_COLOR_SWATCH, mBackgroundColor);
        bundle.putSerializable(Constants.MOVIE_PLAYING_CINEMA, mBoxOfficeMovie);
        mFragment = new GeoMapFragment();
        mFragment.setArguments(bundle);

        if (mFragment != null) {
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.root_container, mFragment).
                    addToBackStack(null)
                    .commit();

            getActivity().setTitle(mTitles[1]);
        }

    }
}

