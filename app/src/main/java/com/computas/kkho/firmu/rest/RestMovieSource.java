package com.computas.kkho.firmu.rest;

import com.computas.kkho.firmu.model.BoxOfficeMovieWrapper;
import com.computas.kkho.firmu.model.BoxOfficeMovieWrapperNowPlaying;
import com.computas.kkho.firmu.model.BoxOfficeSearchMovieWrapper;
import com.computas.kkho.firmu.model.ConfigurationResponse;
import com.computas.kkho.firmu.model.Credits;
import com.computas.kkho.firmu.model.DiscoverWrapper;
import com.computas.kkho.firmu.model.MovieDetail;
import com.computas.kkho.firmu.model.MovieImageWrapper;
import com.computas.kkho.firmu.model.MovieTrailer;
import com.computas.kkho.firmu.model.ReviewWrapper;
import com.computas.kkho.util.BusProvider;
import com.computas.kkho.util.MovieDbApiUrlInfo;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by kkh on 26.03.2015.
 */
public class RestMovieSource implements RestDataSource {
    public static RestMovieSource INSTANCE;
    private final MovieDbApi moviesDBApi;

    private RestMovieSource() {

        RestAdapter movieAPIRest = new RestAdapter.Builder()
                .setEndpoint(MovieDbApiUrlInfo.URL_MOVIE_DB)
                .setLogLevel(RestAdapter.LogLevel.HEADERS_AND_ARGS)
                .build();
        moviesDBApi = movieAPIRest.create(MovieDbApi.class);
    }

    public static RestMovieSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RestMovieSource();
        }
        return INSTANCE;
    }


    @Override
    public void getShows() {
        //TODO
    }

    @Override
    public void getMovies() {
        moviesDBApi.getPopularMovies(MovieDbApiUrlInfo.API_KEY, retrofitCallBack);
    }

    @Override
    public void getNowPlayingMovies() {
        moviesDBApi.getNowPlayingMovies(MovieDbApiUrlInfo.API_KEY, retrofitCallBack);
    }

    @Override
    public void getSearchedMovie(String query) {
        moviesDBApi.getSearchedMovie(MovieDbApiUrlInfo.API_KEY, query, retrofitCallBack);
    }

    @Override
    public void getDetailMovie(String id) {
        moviesDBApi.getMovieDetail(MovieDbApiUrlInfo.API_KEY, id, retrofitCallBack);
    }

    @Override
    public void getMovieTrailer(String id) {
        moviesDBApi.getMovieTrailer(MovieDbApiUrlInfo.API_KEY, id, retrofitCallBack);
    }

    @Override
    public void getReviews(String id) {
        moviesDBApi.getReviews(MovieDbApiUrlInfo.API_KEY, id, retrofitCallBack);
    }

    @Override
    public void getConfiguration() {
        moviesDBApi.getConfiguration(MovieDbApiUrlInfo.API_KEY, retrofitCallBack);
    }

    @Override
    public void getImages(String movieId) {
        moviesDBApi.getImages(MovieDbApiUrlInfo.API_KEY, movieId, retrofitCallBack);
    }

    @Override
    public void getCredits(String movieId) {
        moviesDBApi.getCreditsList(MovieDbApiUrlInfo.API_KEY, movieId, retrofitCallBack);
    }

    public Callback retrofitCallBack = new Callback() {

        @Override
        public void success(Object o, Response response) {
            if (o instanceof MovieDetail) {
                MovieDetail detailResponse = (MovieDetail) o;
                BusProvider.getRestBusInstance().post(detailResponse);
            } else if (o instanceof MovieTrailer) {
                MovieTrailer movieTrailerResponse = (MovieTrailer) o;
                BusProvider.getRestBusInstance().post(movieTrailerResponse);
            } else if (o instanceof BoxOfficeMovieWrapper) {
                BoxOfficeMovieWrapper movieApiResponse = (BoxOfficeMovieWrapper) o;
                BusProvider.getRestBusInstance().post(movieApiResponse);
            } else if (o instanceof BoxOfficeSearchMovieWrapper) {
                BoxOfficeSearchMovieWrapper movieApiResponse = (BoxOfficeSearchMovieWrapper) o;
                BusProvider.getRestBusInstance().post(movieApiResponse);
            } else if (o instanceof BoxOfficeMovieWrapperNowPlaying) {
                BoxOfficeMovieWrapperNowPlaying movieApiResponse = (BoxOfficeMovieWrapperNowPlaying) o;
                BusProvider.getRestBusInstance().post(movieApiResponse);
            } else if (o instanceof DiscoverWrapper) {
                DiscoverWrapper movieApiResponse = (DiscoverWrapper) o;
                BusProvider.getRestBusInstance().post(movieApiResponse);
            } else if (o instanceof ReviewWrapper) {
                ReviewWrapper reviewWrapper = (ReviewWrapper) o;
                BusProvider.getRestBusInstance().post(reviewWrapper);
            } else if (o instanceof MovieImageWrapper) {
                MovieImageWrapper imageWrapper = (MovieImageWrapper) o;
                BusProvider.getRestBusInstance().post(imageWrapper);
            } else if (o instanceof ConfigurationResponse) {
                ConfigurationResponse configurationResponse = (ConfigurationResponse) o;
                BusProvider.getRestBusInstance().post(configurationResponse);
            } else if (o instanceof Credits) {
                Credits credits = (Credits) o;
                BusProvider.getRestBusInstance().post(credits);
            }
        }

        @Override
        public void failure(RetrofitError error) {

        }
    };


    @Override
    public void getMoviesByPage(int page) {
        moviesDBApi.getPopularMoviesByPage(MovieDbApiUrlInfo.API_KEY,
                page + ",",
                retrofitCallBack);
    }

    @Override
    public void getNowPlayingMovieByPage(int page) {
        moviesDBApi.getNowPlayingMoviesByPage(MovieDbApiUrlInfo.API_KEY,
                page + ",",
                retrofitCallBack);
    }

    @Override
    public void getDiscoveryMoviesByPage(int page, int year) {
        moviesDBApi.getDiscoveryMoviesByPage(MovieDbApiUrlInfo.API_KEY,
                year,
                page + ",",
                retrofitCallBack);
    }

    @Override
    public void getSearchedMovieByPage(String query, int page) {
        moviesDBApi.getSearchedMoviesByPage(MovieDbApiUrlInfo.API_KEY,
                query,
                page + ",",
                retrofitCallBack);
    }
}
