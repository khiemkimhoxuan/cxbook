package com.computas.kkho.firmu.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.computas.kkho.firmu.R;
import com.computas.kkho.firmu.model.TheaterMovie;
import com.computas.kkho.firmu.util.ColorUtil;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by kkh on 04.05.2015.
 */
public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieHolder> {
    List<TheaterMovie> mMovieList;
    Context mContext;
    ProgressBar mProgressBar;

    public MovieAdapter(Context context, List<TheaterMovie> movieList, ProgressBar progressBar) {
        mContext = context;
        this.mMovieList = movieList;
        mProgressBar = progressBar;
    }

    public List<TheaterMovie> getMovieList() {
        return mMovieList;
    }


    @Override
    public MovieHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_movies_cinema_info, parent, false);
        return new MovieHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MovieHolder holder, int position) {
        TheaterMovie theaterMovie = mMovieList.get(position);
        holder.vMovieNameTextView.setText(theaterMovie.getMovieName());
        holder.vMovieTimeTextView.setText(theaterMovie.getShowTime());
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return mMovieList.size();
    }

    public class MovieHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.cinema_moviename)
        TextView vMovieNameTextView;
        @InjectView(R.id.cinema_time)
        TextView vMovieTimeTextView;
        @InjectView(R.id.cinema_movieicon)
        ImageView vMovieIcon;
        @InjectView(R.id.cinema_timemovieicon)
        ImageView vTimeIcon;

        public MovieHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
            ColorUtil.colorIcons(vMovieIcon, mContext, R.color.theme_color);
            ColorUtil.colorIcons(vTimeIcon, mContext, R.color.theme_color);
        }
    }
}
