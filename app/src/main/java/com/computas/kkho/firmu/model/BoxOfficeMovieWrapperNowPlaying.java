package com.computas.kkho.firmu.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by kkh on 18.04.2015.
 */
public class BoxOfficeMovieWrapperNowPlaying implements Serializable {
    private Number page;

    private List<BoxOfficeMovie> results;

    private Number total_pages;
    private Number total_results;

    public BoxOfficeMovieWrapperNowPlaying(List<BoxOfficeMovie> results) {

        this.results = results;
    }

    public Number getPage() {

        return this.page;
    }

    public void setPage(Number page) {

        this.page = page;
    }

    public List<BoxOfficeMovie> getResults() {

        return results;
    }

    public Number getTotal_pages() {

        return this.total_pages;
    }

    public void setTotal_pages(Number total_pages) {

        this.total_pages = total_pages;
    }

    public Number getTotal_results() {

        return this.total_results;
    }

    public void setTotal_results(Number total_results) {

        this.total_results = total_results;
    }
}
