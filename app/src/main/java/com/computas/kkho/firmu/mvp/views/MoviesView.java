package com.computas.kkho.firmu.mvp.views;

import com.computas.kkho.firmu.model.BoxOfficeMovie;

import java.util.List;

/**
 * Created by kkh on 26.03.2015.
 */
public interface MoviesView {
    void showMovies(List<BoxOfficeMovie> movieList);

    void showLoading();

    void hideLoading();

    void showError(String error);

    void hideError();

    void showLoadingLabel();

    void hideActionLabel();

    boolean isTheListEmpty();

    void appendMovies(List<BoxOfficeMovie> movieList);
}
