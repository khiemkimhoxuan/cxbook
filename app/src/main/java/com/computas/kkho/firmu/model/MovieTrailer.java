package com.computas.kkho.firmu.model;

import java.util.ArrayList;

/**
 * Created by kkho on 23.06.2015.
 */
public class MovieTrailer {
    private int id;

    private ArrayList<Result> results;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<Result> getResults() {
        return results;
    }

    public void setResults(ArrayList<Result> results) {
        this.results = results;
    }

    public class Result {
        private String id;
        private String iso_639_1;
        private String key;
        private String name;
        private String site;
        private int size;
        private String type;


        public String getId() {
            return this.id;
        }

        public void setId(String id) {
            this.id = id;
        }


        public String getIso6391() {
            return this.iso_639_1;
        }

        public void setIso6391(String iso_639_1) {
            this.iso_639_1 = iso_639_1;
        }


        public String getKey() {
            return this.key;
        }

        public void setKey(String key) {
            this.key = key;
        }


        public String getName() {
            return this.name;
        }

        public void setName(String name) {
            this.name = name;
        }


        public String getSite() {
            return this.site;
        }

        public void setSite(String site) {
            this.site = site;
        }


        public int getSize() {
            return this.size;
        }

        public void setSize(int size) {
            this.size = size;
        }


        public String getType() {
            return this.type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

}
