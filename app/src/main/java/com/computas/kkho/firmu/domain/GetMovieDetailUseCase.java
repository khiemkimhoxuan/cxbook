package com.computas.kkho.firmu.domain;

import com.computas.kkho.firmu.model.Credits;
import com.computas.kkho.firmu.model.MovieDetail;
import com.computas.kkho.firmu.model.MovieImageWrapper;
import com.computas.kkho.firmu.model.ReviewWrapper;

/**
 * Created by kkh on 26.03.2015.
 */
public interface GetMovieDetailUseCase extends UseCase {
    public void requestMovieDetail(String movieId);

    public void requestMovieTrailer(String movieId);

    public void requestMovieReviews(String movieId);

    public void requestMovieImages(String movieId);

    public void requestCreditsResponse(String movieId);

    public void onMovieDetailResponse(MovieDetail response);

    public void onMovieReviewsResponse(ReviewWrapper reviewsWrapper);

    public void onMovieImagesResponse(MovieImageWrapper imageWrapper);

    public void onMovieCreditsResponse(Credits credits);

    public void sendDetailMovieToPresenter(MovieDetail response);
}
