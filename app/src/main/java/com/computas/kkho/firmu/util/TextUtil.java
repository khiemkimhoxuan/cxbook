package com.computas.kkho.firmu.util;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;

import java.text.Normalizer;

/**
 * Created by kkh on 23.03.2015.
 */
public class TextUtil {

    public static String unaccent(String s) {
        String normalized = Normalizer.normalize(s, Normalizer.Form.NFD);
        return normalized.replaceAll("[^\\p{ASCII}]", "");
    }

    public static Spannable setSpannableText(int titleTextColor, CharSequence title) {
        Spannable text = new SpannableString(title);
        text.setSpan(new ForegroundColorSpan(titleTextColor), 0, text.length(),
                Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        return text;
    }

}
