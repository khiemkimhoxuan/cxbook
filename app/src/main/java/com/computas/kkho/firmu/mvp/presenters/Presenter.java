package com.computas.kkho.firmu.mvp.presenters;

/**
 * Created by kkh on 26.03.2015.
 */
public abstract class Presenter {
    public abstract void start();

    public abstract void stop();
}
