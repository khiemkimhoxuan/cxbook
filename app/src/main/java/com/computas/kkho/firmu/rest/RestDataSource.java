package com.computas.kkho.firmu.rest;

/**
 * Created by kkh on 26.03.2015.
 */
public interface RestDataSource extends MediaDataSource {
    public void getMoviesByPage(int page);

    public void getNowPlayingMovieByPage(int page);

    public void getDiscoveryMoviesByPage(int page, int year);

    public void getSearchedMovieByPage(String query, int page);
}
