package com.computas.kkho.firmu.widget;

/**
 * Created by kkh on 03.05.2015.
 */

/**
 * Constants that indicates the scroll state of the Scrollable widgets.
 */
public enum ScrollState {
    /**
     * Widget is stopped.
     * This state does not always mean that this widget have never been scrolled.
     */
    STOP,

    /**
     * Widget is scrolled up by swiping it down.
     */
    UP,

    /**
     * Widget is scrolled down by swiping it up.
     */
    DOWN,
}